<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model {

    use HasFactory, SoftDeletes;

    const ID = 'id';
    const USER_ID = 'user_id';
    const CUST_NAME = 'cust_name';
    const TYPE_PAYMENT = 'type_payment';
    const TOTAL_PRICE = 'total_price';
    const DISCOUNT = 'discount';
    const FINAL_AMOUNT = 'final_amount';
    const PAID_AMOUNT = 'paid_amount';
    const CHANGE = 'change';
    const NOTE = 'note';
    const STATUS = 'status';

    protected $guarded = [];
    protected $primaryKey = Self::ID;
    public $incrementing = false;
    protected $keyType = 'string';

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function forceDelete() {
        $this->items()->delete();
        return parent::forceDelete();
    }

    function user() {
        return $this->belongsTo(User::class);
    }

    function items() {
        return $this->hasMany(SaleItem::class);
    }

    function scopeGenerateSaleTransactionId() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $id = 'TRX-';

        for ($i = 0; $i < 12; $i++) 
            $id .= $characters[random_int(0, strlen($characters) - 1)];

        if ($this->saleTransactionIdExists($id)) return $this->scopeGenerateSaleTransactionId();

        return $id;
    }

    function saleTransactionIdExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }
}

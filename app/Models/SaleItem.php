<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model {

    use HasFactory;

    const ID = 'id';
    const TYPE_ID = 'type_id';
    const SALE_ID = 'sale_id';
    const NAME = 'name';
    const DESC = 'desc';
    const CODE = 'code';
    const QTY = 'qty';
    const CAPITAL_PRICE = 'capital_price';
    const PRICE = 'price';
    const INCOME = 'income';
    const STATUS = 'status';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function sale() {
        return $this->belongsTo(Sale::class);
    }

    function product() {
        return $this->belongsTo(ProductType::class, Self::TYPE_ID);
    }
}

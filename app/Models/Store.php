<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Store extends Model {

    use HasFactory;

    const ID = 'id';
    const NAME = 'name';
    const OWNER = 'owner';
    const DESC = 'desc';
    const ADDRESS = 'address';
    const MOBILE = 'mobile_number';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function users() {
        return $this->hasMany(User::class);
    }

    function purchases() {
        return $this->hasMany(Purchase::class);
    }

    function categories() {
        return $this->hasMany(Category::class);
    }

    function suppliers() : HasMany {
        return $this->hasMany(Supplier::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use HasFactory, Notifiable, SoftDeletes;

    const ID = 'id';
    const STORE_ID = 'store_id';
    const USERNAME = 'username';
    const FULLNAME = 'fullname';
    const ROLE = 'role';
    const PASSWORD = 'password';
    const GENDER = 'gender';
    const RELIGION = 'religion';
    const PLACE_BIRTH = 'place_birth';
    const DATE_BIRTH = 'date_birth';
    const REM_TOKEN = 'remember_token';

    const ROLE_CASHIER = 'cashier';
    const ROLE_OWNER = 'owner';

    protected $guarded = [Self::REM_TOKEN];

    protected $hidden = [Self::PASSWORD, Self::REM_TOKEN];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function sales() {
        return $this->hasMany(Sale::class);
    }

    function store() {
        return $this->belongsTo(Store::class);
    }

    // function wholesale() {
    //     return $this->hasMany(Wholesale::class);
    // }

    // public function user() {
    //     return $this->morphMany(LogActivity::class, 'userable');
    // }

    function returnProducts() {
        return $this->hasMany(ReturnProduct::class);
    }
}

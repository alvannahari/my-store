<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Supplier extends Model {

    use HasFactory;

    const ID = 'id';
    const STORE_ID = 'store_id';
    const FULLNAME = 'fullname';
    const GENDER = 'gender';
    const ADDRESS = 'address';
    const IDENTITY_ID = 'identity_id';
    const DESC = 'desc';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function purchases() {
        return $this->hasMany(Purchase::class);
    }

    function store() : BelongsTo {
        return $this->belongsTo(Store::class);
    }
}

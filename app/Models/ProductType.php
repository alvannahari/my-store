<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use DateTimeInterface;

class ProductType extends Model {

    use HasFactory, LogsActivity;

    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const TYPE = 'type';
    const INVENTORY = 'inventory';
    const CAPITAL_PRICE = 'capital_price';

    const PRICE = 'price';
    const RETAIL = 'Satuan';

    protected $guarded = [];

    protected $appends = [Self::PRICE];

    protected static $recordEvents = ['updated','deleted'];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function prices() {
        return $this->hasMany(ProductPrice::class, ProductPrice::TYPE_ID)->orderBy(ProductPrice::CREATED_AT, 'desc');
    }

    function saleItems() {
        return $this->hasMany(SaleItem::class, SaleItem::TYPE_ID);
    }

    function product() {
        return $this->belongsTo(Product::class);
    }

    function getPriceAttribute() {
        $price = $this->prices()->where(ProductPrice::MIN, '1')->latest()->first()->price ?? '-';
        return $price;
    }

    function decrementStok($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] - $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }

    function incrementStok($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] + $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }

    public function getDescriptionForEvent(string $eventName): string {
        return $eventName.' the data product';
    }

    public function getActivitylogOptions(): LogOptions {
        return LogOptions::defaults()
        ->useLogName('Product Type')
        ->logOnly([Self::CAPITAL_PRICE])
        ->logOnlyDirty();
        // Chain fluent methods for configuration options
    }
}

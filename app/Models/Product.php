<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use DateTimeInterface;

class Product extends Model {

    use HasFactory, SoftDeletes, LogsActivity;

    const IMAGE_PATH = 'product/';

    const ID = 'id';
    const CATEGORY_ID = 'category_id';
    const NAME = 'name';
    const CODE = 'code';
    const DESC = 'desc';
    const IMAGE = 'image';
    
    // const CAPITAL_PRICE = 'capital_price';
    // const PRICE = 'price';
    // const INVENTORY = 'inventory';

    protected $guarded = [];

    public $incrementing = false;

    protected $keyType = 'string';

    // protected $appends = [Self::PRICE, Self::INVENTORY, Self::CAPITAL_PRICE];

    // event will get logged automatically
    protected static $recordEvents = ['created','updated','deleted'];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function forceDelete() {
        if (Storage::disk('public')->exists(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]))
            Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
            
        return parent::forceDelete();
    }

    function category() {
        return $this->belongsTo(Category::class);
    }

    function types() {
        return $this->hasMany(ProductType::class)->latest();
    }

    function returnProducts() {
        return $this->hasMany(ReturnProduct::class);
    }

    function scopeGenerateProductId() {
        // $store_code = str_pad($store_code, 2, "0", STR_PAD_LEFT );
        // $category_id = str_pad($category_id, 3, "0", STR_PAD_LEFT );
        // $id = 'ITEM'.$store_code.'-'.$category_id.substr(str_shuffle('0123456789'),1,5);
        
        $id = 'ITEM-'.substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,12);

        if ($this->productIdExists($id)) return $this->generatePaymentId();

        return $id;
    }

    function productIdExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }

    // function getPriceAttribute() {
    //     $price = $this->types()->where(ProductType::TYPE, 'Satuan')->latest()->first()->price;
    //     return $price;
    // }

    // function getInventoryAttribute() {
    //     return $this->types()->where(ProductType::TYPE, 'Satuan')->latest()->first()->inventory;
    // }

    // function getCapitalPriceAttribute() {
    //     return $this->types()->where(ProductType::TYPE, 'Satuan')->latest()->first()->capital_price;
    // }

    function getImageAttribute($value) {
        if ($value == null) return asset('assets/img/not_found.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
    }

    public function getDescriptionForEvent(string $eventName): string {
        return $eventName.' the data product';
    }

    public function getActivitylogOptions(): LogOptions {
        return LogOptions::defaults()
        ->useLogName('Product')
        ->logOnly([Self::CATEGORY_ID, Self::NAME, Self::DESC])
        ->logOnlyDirty();
        // Chain fluent methods for configuration options
    }
}

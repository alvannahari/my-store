<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use DateTimeInterface;

class ProductPrice extends Model {

    use HasFactory, LogsActivity;

    const ID = 'id';
    const TYPE_ID = 'type_id';
    const MIN = 'min';
    const PRICE = 'price';

    protected $guarded = [];
    
    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function type() {
        return $this->belongsTo(ProductType::class, Self::TYPE_ID);
    }

    public function getDescriptionForEvent(string $eventName): string {
        return $eventName.' the price product';
    }

    public function getActivitylogOptions(): LogOptions {
        return LogOptions::defaults()
        ->useLogName('Price')
        ->logOnly([Self::MIN, Self::PRICE]);
        // Chain fluent methods for configuration options
    }
}

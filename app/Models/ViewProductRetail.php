<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ViewProductRetail extends Model {

    use HasFactory;

    const IMAGE_PATH = 'product/';

    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const NAME = 'name';
    const CODE = 'code';
    const CATEGORY = 'category';
    const STORE_ID = 'store_id';
    const STORE = 'store';
    const DESC = 'desc';
    const IMAGE = 'image';
    const TYPE = 'type';
    const INVENTORY = 'inventory';
    const CAPITAL_PRICE = 'capital_price';
    const MIN = 'min';
    const PRICE = 'price';

    protected $table = "product_retails";
    public $incrementing = false;
    protected $keyType = 'string';

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function getImageAttribute($value) {
        if ($value == null) return asset('assets/img/not_found.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
    }

    function scopeGetProductRetails($query, $store_id = null) {
        $store_id = auth()->guard('admin')->check() ? '' : $store_id;
        return Cache::remember('store_'.$store_id.'_retails', 3600, function () use ($store_id) {
            $query = DB::table('product_retails as pr')
            ->select('pr.*')
            ->leftJoin('product_retails as pr1', function ($join) {
                $join->on('pr.product_id', '=', 'pr1.product_id')
                ->whereRaw(DB::raw('pr.updated_at < pr1.updated_at'));
            })
            ->whereNull('pr1.product_id')
            ->when($store_id, fn ($q, $store_id) => $q->where('pr.store_id', $store_id))
            ->orderBy('pr.name')->get()->toArray();

            return Self::hydrate($query);
        });

        // $query = DB::table('product_retails as pr')
        // ->select('pr.*')
        // ->leftJoin('product_retails as pr1', function ($join) {
        //     $join->on('pr.product_id', '=', 'pr1.product_id')
        //     ->whereRaw(DB::raw('pr.updated_at < pr1.updated_at'));
        // })
        // ->whereNull('pr1.product_id')
        // ->when($store_id, fn ($q, $store_id) => $q->where('pr.store_id', $store_id))
        // ->orderBy('pr.name')->get()->toArray();

        // return Self::hydrate($query);
    }

    function scopeGetProductPrice($query, $store_id = null, $search = null) { 
        if ($store_id != null) {
            $query_product_id = DB::select('
                SELECT p.id as product_id FROM categories c
                LEFT JOIN products p ON c.id = p.category_id
                WHERE c.store_id = '.$store_id.' AND p.id IS NOT NULL
            ');
    
            if (count($query_product_id) == 0) return [];

            $product_ids = json_encode(array_column($query_product_id, 'product_id'));
            $product_ids = $this->_convertArrayToStringArray($product_ids);
        }

        // $product_ids = implode('|', array_column($query_product_id, 'product_id'));

        // $search = explode(' ', $search);
        // $search_key = implode('|', $search);
        $search_key = $search != null ? str_replace(' ','|', $search) : $search;
        $search = $search != null ? '%'.str_replace(' ','%', $search).'%' : $search;

        $query = '
            SELECT pt.id, pt.product_id, p.name, p.code, pt.type, p.image, pt.inventory, pr.price, pt.created_at
            FROM product_types pt 
            LEFT JOIN product_types pt2
            ON (pt.product_id = pt2.product_id AND pt.type = pt2.type AND pt.created_at < pt2.created_at)
            INNER JOIN product_prices pr
            ON pr.id = (SELECT id FROM product_prices WHERE product_prices.type_id = pt.id ORDER BY id DESC LIMIT 1)
            LEFT JOIN products p ON pt.product_id = p.id
            WHERE pt2.product_id IS NULL
        ';

        if ($store_id != null) {
            $query .= " AND pt.product_id IN (".$product_ids.")";
            if ($search != null)
                $query .= " AND (p.name LIKE '".$search."' OR p.code REGEXP '".$search_key."')";
        } else if ($search != null) {
            $query .= " AND (p.name LIKE '".$search."' OR p.code REGEXP '".$search_key."')";
        }

        return DB::select($query .= ' ORDER BY p.name');
    }

    private function _convertArrayToStringArray($data) {
        $result = substr($data, 1);
        $result = substr($result, 0, -1);

        return $result;
    }
}

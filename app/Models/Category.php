<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    use HasFactory;

    const ID = 'id';
    const STORE_ID = 'store_id';
    const NAME = 'name';
    const DESC = 'desc';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->products()->delete();
        return parent::delete();
    }

    function store() {
        return $this->belongsTo(Store::class);
    }

    function products() {
        return $this->hasMany(Product::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PurchaseImage extends Model {

    use HasFactory;

    const IMAGE_PATH = 'purchase/';

    const ID = 'id';
    const PURCHASE_ID = 'purchase_id';
    const IMAGE = 'image';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
        
        return parent::delete();
    }

    function purchase() {
        return $this->belongsTo(Purchase::class);
    }

    function getImageAttribute($value) {
        return asset('storage/'.Self::IMAGE_PATH.$value);
    }
}

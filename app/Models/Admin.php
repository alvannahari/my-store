<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable {

    use HasFactory, Notifiable;

    const ID = 'id';
    const USERNAME = 'username';
    const FULLNAME = 'fullname';
    const PASSWORD = 'password';
    const REM_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [Self::PASSWORD, Self::REM_TOKEN];

    public function user() {
        return $this->morphMany(LogActivity::class, 'userable');
    }
}

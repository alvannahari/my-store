<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnProduct extends Model {

    use HasFactory;

    const ID = '';
    const USER_ID = 'user_id';
    const PRODUCT_ID = 'product_id';
    const NAME = 'name';
    const CODE = 'code';
    const QTY = 'qty';
    const STATUS = 'status';
    const NOTE = 'note';

    protected $guarded = [];

    protected $casts = [
        Self::CREATED_AT => 'datetime:Y-m-d H:i:s',
        Self::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function user() {
        return $this->belongsTo(User::class);
    }

    function product() {
        return $this->belongsTo(Product::class);
    }

}

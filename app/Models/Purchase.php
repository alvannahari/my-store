<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Purchase extends Model {

    use HasFactory;

    const ID = 'id';
    const NAME = 'name';
    const SUPPLIER_ID = 'supplier_id';
    const STORE_ID = 'store_id';
    const SUPPLIER_NAME = 'supplier_name';
    const AMOUNT = 'amount';
    const DATE = 'date';
    const TOTAL_PRICE = 'total_price';

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function images() {
        return $this->hasMany(PurchaseImage::class);
    }

    function store() {
        return $this->belongsTo(Store::class);
    }

    function supplier() {
        return $this->belongsTo(Supplier::class);
    }
}

<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToCollection {

    function collection(Collection $rows) {
        foreach ($rows as $row)  {
            $product_id = Product::generateProductId();

            Product::create([
                Product::ID => $product_id,
                Product::CATEGORY_ID => $row[1],
                Product::CODE => $row[2],
                Product::NAME => $row[3],
                Product::CREATED_AT => Carbon::now(),
                Product::UPDATED_AT => Carbon::now()
            ]);

            $type = ProductType::create([
                ProductType::PRODUCT_ID => $product_id,
                ProductType::TYPE => 'Ecer',
                ProductType::INVENTORY => $row[4],
                ProductType::CAPITAL_PRICE => $row[5],
                ProductType::CREATED_AT => Carbon::now(),
                ProductType::UPDATED_AT => Carbon::now()
            ]);

            ProductPrice::create([
                ProductPrice::TYPE_ID => $type->id,
                ProductPrice::MIN => 1,
                ProductPrice::PRICE => $row[6],
                ProductPrice::CREATED_AT => Carbon::now(),
                ProductPrice::UPDATED_AT => Carbon::now()
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
        $this->validator($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // $this->incrementLoginAttempts($request);
        return redirect()->back()->with('message', 'Email Atau Password Tidak Cocok !!');
    }

    protected function validator(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME      => ['required','alpha_dash'],
            User::PASSWORD   => ['required','string'],
        ], [
            'alpha_dash' => ':attribute tidak boleh mengandung spasi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        };
    }

    public function username() {
        return 'username';
    }
    
    public function attemptLogin(Request $request) {
        foreach(array_keys(config('auth.guards')) as $guard){
            if ($guard == 'admin' && $this->guard($guard)->attempt($this->credentials($request))) {
                session()->put('guard', $guard);
                return true;
                break;
            } else if ($guard != 'api') {
                $user = User::where(User::USERNAME, $request->input(User::USERNAME))->first();

                if ($user && password_verify($request->input(User::PASSWORD), $user->password)) {
                    if ($user->role == 'owner') {
                        Auth::guard('owner')->login($user);
                        session()->put('guard', $user->role);
                    } else {
                        Auth::guard('cashier')->login($user);
                        session()->put('guard', $user->role);
                    }
                    return true;
                    break;
                }
            };
        }
        return false;
    }

    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return redirect()->intended($this->redirectPath());
    }

    public function guard($guard) {
        return Auth::guard($guard);
    }

    public function logout(Request $request) {
        $this->guard(session()->get('guard'))->logout();
        session()->forget('guard');
        // $this->guard(Auth::getDefaultDriver())->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}

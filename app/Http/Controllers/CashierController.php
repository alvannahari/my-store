<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CashierController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check())
                $data = User::with('store')->where(User::ROLE, User::ROLE_CASHIER)->get();
            else 
                $data = User::with('store')->where(User::ROLE, User::ROLE_CASHIER)->where(User::STORE_ID, auth()->user()->store_id)->get();

            return response(['data' => $data]);
        };
        if (auth()->guard('admin')->check())
            $stores = Store::get();
        else
            $stores = auth()->user()->store;
        
        return view('page.cashier.index', compact('stores'));
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME => 'required|alpha_dash|unique:users',
            User::FULLNAME => 'required|string',
            User::PASSWORD => 'required|confirmed|min:6',
            User::RELIGION => 'required',
            User::STORE_ID => 'required',
            User::PLACE_BIRTH => 'required',
            User::DATE_BIRTH => 'required',
            User::STORE_ID => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'error' => $validator->errors()]);

        $payload = $request->all();
        $payload[User::PASSWORD] = bcrypt($payload[User::PASSWORD]);
        $payload[User::ROLE] = User::ROLE_CASHIER;
        User::create($payload);

        session()->flash('status', 'Akun Kasir');
        session()->flash('message', 'baru berhasil tersimpan');

        return response(['status' => true]); 
    }

    function show(User $cashier) {
        $stores = Store::get();

        return view('page.cashier.detail', compact('cashier','stores'));
    }

    function update(Request $request, User $cashier) {
        $payload = $request->except(User::PASSWORD, 'password_confirmation');

        $rules = [
            User::USERNAME => 'required|alpha_dash|unique:users,username,'.$cashier->id,
            User::FULLNAME => 'required|string',
            User::RELIGION => 'required',
            User::PLACE_BIRTH => 'required',
            User::DATE_BIRTH => 'required'
        ];
        
        if ($request->filled('password')) {
            $rules[User::PASSWORD] = 'required|confirmed|min:6';
            $payload[User::PASSWORD] = bcrypt($payload[User::PASSWORD]);
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response(['status' => false,'error' => $validator->errors()]);

        $cashier->update($payload);

        session()->flash('status', 'Akun Kasir');
        session()->flash('message', 'berhasil diperbarui');

        return response(['status' => true]);
    }

    function destroy(User $cashier) {
        $cashier->delete();

        return redirect()->back()->with(['status' => 'Akun Kasir', 'message' => 'berhasil terhapus']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller {

    public function index() {
        if (request()->ajax()) {
            $data = Supplier::get();
            return response(['data' => $data]);
        }
        return view('page.supplier.index');
    }

    public function store(Request $request) {
        $credentials = $request->all();
        Supplier::create($credentials);

        return redirect()->back()->with(['status' => 'Supplier', 'message' => 'baru berhasil tersimpan.']);
    }

    public function show(Supplier $supplier) {
        return view('page.supplier.detail', compact('supplier'));
    }

    public function update(Request $request, Supplier $supplier) {
        $credentials = $request->all();
        $supplier->update($credentials);

        return redirect()->route('supplier.index')->with(['status' => 'Supplier', 'message' => 'data berhasil diperbarui.']);
    }

    public function destroy(Supplier $supplier) {
        $supplier->delete();

        return redirect()->back()->with(['status' => 'Supplier', 'message' => 'data berhasil terhapus.']);
    }
}

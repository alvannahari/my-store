<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\PurchaseImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PurchaseImageController extends Controller {
    
    function store(Request $request) {
        $payload = $request->only(PurchaseImage::PURCHASE_ID);

        DB::beginTransaction();

        try {
            $image = Storage::disk('public')->put(PurchaseImage::IMAGE_PATH, $request->file(PurchaseImage::IMAGE));
            $payload[PurchaseImage::IMAGE] = basename($image);

            PurchaseImage::create($payload);

            DB::commit();
            return redirect()->back()->with(['status' => 'File pembelian', 'message' => 'Gambar berhasil tersimpan']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    function destroy(PurchaseImage $purchaseImage) {
        $purchaseImage->delete();

        return redirect()->back()->with(['status' => 'File pembelian', 'message' => 'Gambar berhasil terhapus']);
    }
}

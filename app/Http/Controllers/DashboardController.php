<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Sale;
use App\Models\SaleItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {
    
    function index() {
        if (auth()->guard('cashier')->check()) {
            foreach ($this->_dailyDataCashier() as $key => $value) 
                ${$key} = $value;
            
            return view('page.dashboard.cashier', compact('daily','sellers','sold'));
        } else if (auth()->guard('owner')->check()) {
            foreach ($this->_dailyDataOwner() as $key => $value) 
                ${$key} = $value;

            return view('page.dashboard.owner', compact('daily','sellers','sold'));
        } else {
            return view('page.dashboard.index');
        }

        return view('dashboard');
    }

    function getPercent($value, $compare) {
        $divide = $compare > 0 ? $compare : 1; // set value if compare/divide has 0

        $percent = ($value - $compare) / $divide * 100; // counting to result percent

        $percent = $percent <= 1000 ? $percent : 1000 ; // set max percent

        $string = $value >= $compare ? '+' : ''; // set desc + for positive result

        return  $string.number_format($percent, 1, '.', '').'%';
    }
    
    function graphWeekly(array $activity) {
        $data['label'] = [
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu','Minggu'
        ];

        for ($i=0; $i<7; $i++){
            $temp = false;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['day'] == $thisDate){
                    $data['revenue'][$i] = (int) $value['revenue'];
                    $data['income'][$i] = (int) $value['income'];
                    $temp = true;
                    break;
                };
            };
            if(!$temp) {
                $data['revenue'][$i] = 0;
                $data['income'][$i] = 0;
            }
        };

        return $data;
    }

    private function _getDataOwner() {
        $total['sales'] = Sale::count();
        $total['items'] = SaleItem::count();
        $total['revenue'] = Sale::sum(Sale::TOTAL_PRICE);
        $total['income'] = SaleItem::sum(SaleItem::INCOME);

        // Grafik Pemasukan Mingguan
        $weekly = SaleItem::select(
            DB::raw('sum(price * qty) as revenue'), 
            DB::raw('sum(income) as income'), 
            DB::raw("DATE_FORMAT(created_at,'%d') as day")
        )
        ->whereBetween(SaleItem::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->groupBy('day')
        ->orderBy(Sale::CREATED_AT)->get()->toArray();
        $graph['weekly'] = $this->graphWeekly($weekly);

        // Data Ringkasan Harian
        $today['sale'] = Sale::whereDate(Sale::CREATED_AT, Carbon::today())->count();
        $today['sale_percent'] = $this->getPercent($today['sale'], Sale::whereDate(Sale::CREATED_AT, Carbon::yesterday())->count());
        $today['items'] = SaleItem::whereDate(SaleItem::CREATED_AT, Carbon::today())->count();
        $today['items_percent'] = $this->getPercent($today['items'], SaleItem::whereDate(SaleItem::CREATED_AT, Carbon::yesterday())->count());
        $today['revenue'] = Sale::whereDate(Sale::CREATED_AT, Carbon::today())->sum(Sale::TOTAL_PRICE);
        $today['revenue_percent'] = $this->getPercent($today['revenue'], Sale::whereDate(Sale::CREATED_AT, Carbon::yesterday())->sum(Sale::TOTAL_PRICE));
        $today['income'] = SaleItem::whereDate(SaleItem::CREATED_AT, Carbon::today())->sum(SaleItem::INCOME);
        $today['income_percent'] = $this->getPercent($today['income'], SaleItem::whereDate(SaleItem::CREATED_AT, Carbon::yesterday())->sum(SaleItem::INCOME));
        $today['discount'] = Sale::whereDate(Sale::CREATED_AT, Carbon::today())->sum(Sale::DISCOUNT);

        // Data Pendapatan
        $revenue['daily'] = $today['revenue'];
        $revenue['daily_status'] = substr($today['revenue_percent'], 0, 1) == '+' ? true : false;
        $revenue['weekly'] = Sale::whereBetween(Sale::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum(Sale::TOTAL_PRICE);
        $revenue['weekly_status'] = substr($today['revenue_percent'], 0, 1) == '+' ? true : false; //THIS FORMULA NOT FINISHED
        $revenue['monthly'] = Sale::whereMonth(Sale::CREATED_AT, Carbon::now()->month)->sum(Sale::TOTAL_PRICE);
        $revenue['monthly_status'] = $revenue['monthly'] > Sale::whereMonth(Sale::CREATED_AT, Carbon::now()->subMonth()->month)->sum(Sale::TOTAL_PRICE) ? true : false;
        $revenue['yearly'] = Sale::whereYear(Sale::CREATED_AT, Carbon::now()->year)->sum(Sale::TOTAL_PRICE);
        $revenue['yearly_status'] = $revenue['yearly'] > Sale::whereYear(Sale::CREATED_AT, Carbon::now()->subYear()->year)->sum(Sale::TOTAL_PRICE) ? true : false;

        // Grafik Bulanan
        $monthly = SaleItem::select(
            DB::raw('sum(price * qty) as revenue'), 
            DB::raw('sum(income) as income'), 
            DB::raw("DATE_FORMAT(created_at,'%M') as month")
        )
        ->whereYear(Sale::CREATED_AT, Carbon::now()->year)->groupBy('month')
        ->orderBy(Sale::CREATED_AT)->get()->toArray();
        $graph['monthly']['revenue'] = array_column($monthly, 'revenue');
        $graph['monthly']['income'] = array_column($monthly, 'income');
        $graph['monthly']['month'] = array_column($monthly, 'month');

        // Grafik Tahunan
        $monthly = SaleItem::select(
            DB::raw('sum(price * qty) as revenue'), 
            DB::raw('sum(income) as income'), 
            DB::raw("DATE_FORMAT(created_at,'%Y') as year")
        )
        ->orderBy(Sale::CREATED_AT)->groupBy('year')->get()->toArray();
        $graph['yearly']['revenue'] = array_column($monthly, 'revenue');
        $graph['yearly']['income'] = array_column($monthly, 'income');
        $graph['yearly']['year'] = array_column($monthly, 'year');
        
        // Best Seller dan history produk terjual
        $store = auth()->user()->store;
        $sold = SaleItem::whereHas('sale', function($q) use ($store) {
            $q->whereIn(Sale::USER_ID, $store->users->pluck(User::ID));
        })->orderBy(SaleItem::CREATED_AT)->get();

        // Perhitungan persen untuk produk terlaris belum berdasarkan seluruh kuantitas produk terjual
        // Perhitungan ketika penjualan terhapus dan bernilai null berlum selesai

        // $best = Product::whereHas('category', function($q) use ($store) {
        //     $q->where(Category::STORE_ID, $store->id);
        // })->withCount('saleItems')->having('sale_items_count', '>', 0)->orderBy('sale_items_count', 'desc')->get()->toArray();
        // $sellers = [];
        // foreach (array_slice($best, 0, 10) as $key => $value) {
        //     $product = $value;
        //     $product['percent'] = $value['sale_items_count'] / count($sold) * 100;
        //     $sellers[] = $product;
        // }

        $best = ProductType::with('product')->whereHas('product.category', function($q) use ($store) {
            $q->where(Category::STORE_ID, $store->id);
        })->withCount('saleItems')->having('sale_items_count', '>', 0)->orderBy('sale_items_count', 'desc')->get()->toArray();
        $sellers = [];
        foreach (array_slice($best, 0, 10) as $key => $value) {
            $product = $value;
            $percent = $value['sale_items_count'] / count($sold) * 100;
            $product['percent'] = number_format($percent, 1, '.', '');
            $sellers[] = $product;
        }

        return array('total' => $total, 'weekly' => $weekly, 'today' => $today, 'revenue' => $revenue, 'graph' => $graph, 'sellers' => $sellers, 'sold' => $sold->take(10));
    }

    private function _dailyDataOwner() {
        $store = auth()->user()->store;
        $daily['items'] = Product::whereHas('category', function ($q) use ($store) {
            $q->where(Category::STORE_ID, $store->id);
        })->count();
        $daily['sales'] = Sale::whereHas('user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->whereDate(Sale::CREATED_AT, Carbon::now())->count();
        $daily['revenue'] = Sale::whereHas('user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->whereDate(Sale::CREATED_AT, Carbon::now())->sum(Sale::TOTAL_PRICE);
        $daily['income'] = SaleItem::whereHas('sale.user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->whereDate(Sale::CREATED_AT, Carbon::now())->sum(SaleItem::INCOME);

        // Best Seller dan history produk terjual
        $sold = SaleItem::whereHas('sale.user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->orderBy(SaleItem::CREATED_AT)->get();

        $best = ProductType::with('product')->whereHas('product.category', function($q) use ($store) {
            $q->where(Category::STORE_ID, $store->id);
        })->withCount('saleItems')->having('sale_items_count', '>', 0)->orderBy('sale_items_count', 'desc')->get()->toArray();
        $sellers = [];
        foreach (array_slice($best, 0, 10) as $key => $value) {
            $product = $value;
            $percent = $value['sale_items_count'] / count($sold) * 100;
            $product['percent'] = number_format($percent, 1, '.', '');
            $sellers[] = $product;
        }

        return array('daily' => $daily, 'sellers' => $sellers, 'sold' => $sold->take(10));
    }

    private function _dailyDataCashier() {
        $store = auth()->user()->store;
        $daily['items'] = Product::whereHas('category', function ($q) use ($store) {
            $q->where(Category::STORE_ID, $store->id);
        })->count();
        $daily['sales'] = Sale::whereHas('user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->whereDate(Sale::CREATED_AT, Carbon::now())->count();
        $daily['revenue'] = Sale::whereHas('user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->whereDate(Sale::CREATED_AT, Carbon::now())->sum(Sale::TOTAL_PRICE);

        // Best Seller dan history produk terjual
        $sold = SaleItem::whereHas('sale.user', function ($q) use ($store) {
            $q->where(User::STORE_ID, $store->id);
        })->orderBy(SaleItem::CREATED_AT)->get();

        $best = ProductType::with('product')->whereHas('product.category', function($q) use ($store) {
            $q->where(Category::STORE_ID, $store->id);
        })->withCount('saleItems')->having('sale_items_count', '>', 0)->orderBy('sale_items_count', 'desc')->get()->toArray();
        $sellers = [];
        foreach (array_slice($best, 0, 10) as $key => $value) {
            $product = $value;
            $percent = $value['sale_items_count'] / count($sold) * 100;
            $product['percent'] = number_format($percent, 1, '.', '');
            $sellers[] = $product;
        }

        return array('daily' => $daily, 'sellers' => $sellers, 'sold' => $sold->take(10));
    }
}

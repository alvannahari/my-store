<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function response_success($message, $data, $code = 200) {
        return response()->json([
            'status' => true,
            'message' => $message, 
            'data' => $data
        ], $code);
    }

    function response_error($message, $data, $code = 500) {
        return response()->json([
            'status' => false,
            'message' => $message, 
            'data' => $data
        ], $code);
    }
}

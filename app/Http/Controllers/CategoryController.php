<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller {

    public function index() {
        if (request()->ajax()) {
            $data = [];
            if (auth()->guard('admin')->check())
                $data = Category::with('store')->withCount('products')->get();
            else 
                $data = Category::with('store')->withCount('products')->where(Category::STORE_ID, auth()->user()->store_id)->get();

            return response(['data' => $data]);
        }
        
        if (auth()->guard('admin')->check()) 
            $stores = Store::get();
        else 
            $stores = auth()->user()->store;
            
        return view('page.category.index', compact('stores'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Category::STORE_ID => 'required',
            Category::NAME => 'required',
            Category::DESC => 'nullable',
        ]);

        if ($validator->fails())
            return response(['status' => false, 'errors' => $validator->errors()]);

        $payload = $validator->validate();
        Category::create($payload);

        return response(['status' => 'Kategori Produk', 'message' => 'berhasil tersimpan.']);
    }

    public function show(Category $category) {
        if (auth()->guard('admin')->check()) 
            $stores = Store::get();
        else 
            $stores = auth()->user()->store;

        $category->loadCount('products');
        return view('page.category.detail', compact('stores','category'));
    }

    public function update(Request $request, Category $category) {
        $validator = Validator::make($request->all(), [
            Category::STORE_ID => 'required',
            Category::NAME => 'required',
            Category::DESC => 'nullable',
        ]);

        if ($validator->fails())
            return response(['status' => false, 'errors' => $validator->errors()]);

        $payload = $validator->validate();
        $category->update($payload);

        return response(['status' => true, 'message' => 'berhasil diperbarui.']);
    }

    public function destroy(Category $category) {
        $category->delete();

        return response(['status' => true, 'message' => 'berhasil terhapus.']);
    }
}

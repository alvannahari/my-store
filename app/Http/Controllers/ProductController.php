<?php

namespace App\Http\Controllers;

use App\Imports\ProductImport;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductType;
use App\Models\Store;
use App\Models\ViewProductRetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller {

    public function index() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) {
                $data = ViewProductRetail::getProductRetails();
            } else {
                $data = ViewProductRetail::getProductRetails(auth()->user()->store_id);
            }
            return response(['data' => $data]);
        }

        if (auth()->guard('cashier')->check()) 
            return view('page.product.cashier');
        else
            return view('page.product.owner');
    }

    function others() {
        if (request()->ajax()) {
            $data = ViewProductRetail::getProductRetails();
            return response(['data' => $data]);
        }
        return view('page.product.index');
    }

    public function create() {
        if (auth()->guard('admin')->check()) {
            $categories = Category::get();
            $stores = Store::get();
        } else {
            $store_id = auth()->user()->store->id;
            $stores = Store::find($store_id);
            $categories = Category::where(Category::STORE_ID, $store_id)->get();
        }

        return view('page.product.create', compact('categories','stores'));
    }

    public function store(Request $request) {
        DB::beginTransaction();
        
        try {
            $product_id = Product::generateProductId();
            $credentials = $request->except(['store_id',ProductType::PRICE,ProductType::CAPITAL_PRICE,ProductType::INVENTORY]);
            $credentials[Product::ID] = $product_id;
            
            if ($request->has(Product::IMAGE)) {
                $image = Storage::disk('public')->put(Product::IMAGE_PATH, $request->file(Product::IMAGE));
                $credentials[Product::IMAGE] = basename($image);
            }
    
            $product = Product::create($credentials);
    
            $type = $product->types()->create([
                ProductType::TYPE => ProductType::RETAIL,
                ProductType::INVENTORY => $request->input(ProductType::INVENTORY),
                ProductType::CAPITAL_PRICE => $request->input(ProductType::CAPITAL_PRICE)
            ]);

            $type->prices()->create([
                ProductPrice::PRICE => $request->input('price'),
                ProductPrice::MIN => 1
            ]);
    
            DB::commit();
            $this->_destroyCacheProductRetails();
            return redirect()->route('product.index')->with(['status' => 'Produk', 'message' => 'baru berhasil tersimpan.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => 'Produk gagal tersimpan.']);
        }
    }

    public function show(Product $product) {
        $stores = Store::get();
        if (auth()->guard('admin')->check()) {
            $categories = Category::get();
        } else {
            $categories = auth()->user()->store->categories;
        }

        $product->load('category.store','types');
        $owner = false;

        if (auth()->guard('owner')->check() && $product->category->store_id == auth()->user()->store->id) {
            $owner = true;
        }

        $types = ProductType::with('prices')->where(ProductType::PRODUCT_ID, $product->id)->latest()->get()->toArray();
        $current = [];
        $histories = [];

        foreach ($types as $key => $prices) {
            foreach ($prices['prices'] as $keyPrice => $valPrice) {
                $price = [
                    ProductPrice::ID => $valPrice[ProductPrice::ID],
                    ProductPrice::MIN => $valPrice[ProductPrice::MIN],
                    ProductType::CAPITAL_PRICE => $prices[ProductType::CAPITAL_PRICE],
                    ProductType::INVENTORY => $prices[ProductType::INVENTORY],
                    ProductPrice::PRICE => $valPrice[ProductPrice::PRICE],
                    ProductPrice::CREATED_AT => $valPrice[ProductPrice::CREATED_AT]
                ];

                if (array_key_exists($prices[ProductType::TYPE], $current)) {
                    if (array_search($valPrice[ProductPrice::MIN], array_column($current[$prices[ProductType::TYPE]], ProductPrice::MIN)) !== FALSE) {
                        $histories[$prices[ProductType::TYPE]][] = $price;
                    } else {
                        $current[$prices[ProductType::TYPE]][] = $price;
                    }
                } else {
                    $current[$prices[ProductType::TYPE]][] = $price;
                }
            }
        }

        return view('page.product.detail', compact('product','categories','stores', 'owner', 'current', 'histories'));
    }

    public function update(Request $request, Product $product) {
        $credentials = $request->except(Product::IMAGE,'store_id');

        if ($request->has(Product::IMAGE)) {
            $arr_image = explode('/',$product->image);
            if (end($arr_image) != 'not_found.png') {
                Storage::disk('public')->delete(Product::IMAGE_PATH.end($arr_image));
            }
            $image = Storage::disk('public')->put(Product::IMAGE_PATH, $request->file(Product::IMAGE));
            $credentials[Product::IMAGE] = basename($image);
        }

        $product->update($credentials);
        $this->_destroyCacheProductRetails();

        return redirect()->route('product.index')->with(['status' => 'Produk', 'message' => 'berhasil diperbarui.']);
    }

    public function destroy(Product $product) {
        $product->delete();
        $this->_destroyCacheProductRetails();

        return redirect()->back()->with(['status' => 'Produk', 'message' => 'berhasil terhapus.']);
    }

    function import(Request $request) {
        DB::beginTransaction();

        try {
            Excel::import(new ProductImport, $request->file('file'));
            
            DB::commit();
            $this->_destroyCacheProductRetails();
            return redirect()->back()->with(['status' => 'File Import', 'message' => 'Data berhasil di Import.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    private function _destroyCacheProductRetails() {
        if (auth()->check('admin')) {
            return Cache::flush();
        } else {
            return Cache::forget('store_'.auth()->user()->store->id.'_retails');
        }
    }
}

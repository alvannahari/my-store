<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductPriceController extends Controller {

    function store(Request $request) {
        DB::beginTransaction();

        try {
            $type = ProductType::where(ProductType::PRODUCT_ID, $request->input(ProductType::PRODUCT_ID))->where(ProductType::TYPE, $request->input(ProductType::TYPE))->where(ProductType::CAPITAL_PRICE, $request->input(ProductType::CAPITAL_PRICE))->first();
    
            if (!$type) {
                $payload = $request->except(ProductPrice::MIN, ProductPrice::PRICE);
                $type = ProductType::create($payload);
            }

            if ($type->inventory != $request->input(ProductType::INVENTORY)) {
                $payload = $request->except(ProductPrice::MIN, ProductPrice::PRICE);
                $type = ProductType::create($payload);
            }
    
            $payload = $request->only(ProductPrice::MIN, ProductPrice::PRICE);
            $type->prices()->create($payload);

            $type->product()->update([
                Product::UPDATED_AT => Carbon::now()
            ]);
    
            DB::commit();
            $this->_destroyCacheProductRetails();
            return redirect()->back()->with(['status' => 'Produk', 'message' => 'Berhasil memperbarui harga.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }
    
    function destroy(ProductPrice $productPrice) {
        if (ProductPrice::where(ProductPrice::TYPE_ID, $productPrice->type_id)->count() == 1) {
            $productPrice->type->delete();
        }

        $productPrice->delete();
        $this->_destroyCacheProductRetails();
        return redirect()->back()->with(['status' => 'Produk', 'message' => 'Berhasil menghapus harga.']);
    }

    private function _destroyCacheProductRetails() {
        if (auth()->check('admin')) {
            return Cache::flush();
        } else {
            return Cache::forget('store_'.auth()->user()->store->id.'_retails');
        }
    }
}

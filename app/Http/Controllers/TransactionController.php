<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductType;
use App\Models\Sale;
use App\Models\SaleItem;
use App\Models\ViewProductRetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller {
    
    function create() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) {
                $products = ViewProductRetail::getProductPrice();
            } else {
                $products = ViewProductRetail::getProductPrice(auth()->user()->store_id, request()->input('search'));
            }

            return response(['status' => true, 'data' => $products]);
        }
        
        return view('page.sale.transaction');
    }

    function store(Request $request) {
        // return response($request->all());
        $validator = Validator::make($request->all(), [
            Sale::TOTAL_PRICE => 'required|numeric',
            Sale::DISCOUNT => 'nullable|numeric',
            Sale::FINAL_AMOUNT => 'required|numeric',
            Sale::PAID_AMOUNT => 'required|numeric',
            Sale::CHANGE => 'required|numeric',
            'items' => 'required|array|min:1'
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'message' => 'Silahkan Periksa Form', 'errors' => $validator->errors()]);

        DB::beginTransaction();
        try {
            $payload = $request->except('items');
            $payload[Sale::ID] = Sale::generateSaleTransactionId();
            $payload[Sale::USER_ID] = auth()->id();
            $payload[Sale::CUST_NAME] = $request->input(Sale::CUST_NAME) ?? '-';
            $sale = Sale::create($payload);

            foreach ($request->input('items') as $key => $value) {
                $item = ProductType::with('product')->find($value['id']);

                if ($item->inventory < $value['qty']) {
                    return response(['status' => false, 'message' => 'Beberapa produk melebihi stok barang.', 'errors' => []]);
                }

                $item->decrementStok($value['qty']);

                $name = $item->type == 'Satuan' ? $item->product->name : $item->product->name.' '.$item->type;

                $payload_item[SaleItem::SALE_ID] = $sale->id;
                $payload_item[SaleItem::TYPE_ID] = $value['id'];
                $payload_item[SaleItem::NAME] = $name;
                $payload_item[SaleItem::DESC] = $item->product->desc;
                $payload_item[SaleItem::CODE] = $item->product->code;
                $payload_item[SaleItem::QTY] = $value['qty'];
                $payload_item[SaleItem::CAPITAL_PRICE] = $item->capital_price;
                $payload_item[SaleItem::PRICE] = $item->price;
                $payload_item[SaleItem::INCOME] = ($value['qty'] * $item->price) - ($value['qty'] * $item->capital_price);
                // return response(['sale' => $sale->id, 'data' => $payload_item]);
                SaleItem::create($payload_item);
            }
    
            DB::commit();
            return response(['status' => true, 'message' => 'baru berhasil tersimpan.', 'data' => $sale]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['status' => false, 'message' => $th->getMessage()]);
        }
    }

    function show($transaction) {
        $transaction = Sale::withTrashed()->findOrFail($transaction);
        $transaction->load('items');

        if (request()->ajax()) {
            if (empty($transaction)) 
                return response(['status' => false]);
            
            return response(['status' => true]);
        }

        return view('page.sale.print', compact('transaction'));
    }
}

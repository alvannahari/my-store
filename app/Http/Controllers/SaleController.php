<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductType;
use App\Models\Sale;
use App\Models\SaleItem;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller {

    public function index() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) {
                $sales = Sale::with('user','items')->get();
            } else {
                $sales = Sale::with('user','items')->whereHas('user', function ($q) {
                    $q->where(User::STORE_ID, auth()->user()->store_id);
                })->get();
            }
            return response(['data' => $sales]);
        }
        return view('page.sale.index');
    }

    function create() { 
        if (request()->ajax()) {
            $products = Product::with('types')->orderBy(Product::NAME)->get()->toArray();
    
            $data = [];
    
            foreach ($products as $product) {
                if (count($product['types']) === 1) {
                    $value = $product['types'][0];
                    $value[Product::NAME] = $product[Product::NAME];
                    $value[Product::CODE] = $product[Product::CODE];
                    // dd($value);
                    $data[] = $value;
                } else {
                    $temp = [];
                    $types = [];
                    foreach ($product['types'] as $type) {
                        if (!in_array(strtolower($type['type']), $temp)) {
                            $temp[] = strtolower($type['type']);
                            $value = $type;
                            $value[Product::NAME] = $product[Product::NAME];
                            $value[Product::CODE] = $product[Product::CODE];
                            $types[] = $value;
                        }
                    }
                    $data = array_merge($data, $types);
                }
            }
    
            return response(['status' => true, 'data' => $data]);
        }
        
        return view('page.sale.create');
    }

    public function store(Request $request) {
        // return response($request->all());
        DB::beginTransaction();
        try {
            $payload = $request->except('item');
            $payload[Sale::USER_ID] = auth()->user()->id;
            $payload[Sale::CUST_NAME] = $request->input(Sale::CUST_NAME) ?? '-';
            $sale = Sale::create($payload);
    
            foreach ($request->input('item') as $key => $value) {
                $item = ProductType::with('product')->find($value['id']);

                if ($item->inventory < $value['qty']) {
                    throw new Exception("Beberapa produk melebihi stok");
                }

                $item->decrementStok($value['qty']);

                $payload_item[SaleItem::SALE_ID] = $sale->id;
                $payload_item[SaleItem::TYPE_ID] = $value['id'];
                $payload_item[SaleItem::NAME] = $item->product->name;
                $payload_item[SaleItem::DESC] = $item->product->desc;
                $payload_item[SaleItem::CODE] = $item->product->code;
                $payload_item[SaleItem::QTY] = $value['qty'];
                $payload_item[SaleItem::PRICE] = $item->price;
                $payload_item[SaleItem::INCOME] = ($value['qty'] * $item->price) - ($value['qty'] * $item->capital_price);
                SaleItem::create($payload_item);
            }
    
            DB::commit();
            
            return redirect()->route('sale.index')->with(['status' => 'Penjualan Produk', 'message' => 'baru berhasil tersimpan.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    public function show($sale) {
        // if (request()->ajax()) {
        //     $products = ProductType::with('product','prices')->get();
        //     return response(['data' => $products]);
        // }
        $sale = Sale::withTrashed()->findOrFail($sale);
        $sale->load('user','items');
        return view('page.sale.show', compact('sale'));
    }

    public function update(Request $request, Sale $sale) {
        // return response($request->all());
        DB::beginTransaction();

        try {
            $sale->items()->delete();

            $payload = $request->except('item');
            $payload[Sale::USER_ID] = auth()->user()->id;
            $payload[Sale::CUST_NAME] = $request->input(Sale::CUST_NAME) ?? '-';
            $sale->update($payload);
    
            foreach ($request->input('item') as $key => $value) {
                $item = ProductType::with('product')->find($value['id']);
                $payload_item[SaleItem::SALE_ID] = $sale->id;
                $payload_item[SaleItem::TYPE_ID] = $value['id'];
                $payload_item[SaleItem::NAME] = $item->product->name;
                $payload_item[SaleItem::DESC] = $item->product->desc;
                $payload_item[SaleItem::CODE] = $item->product->code;
                $payload_item[SaleItem::QTY] = $value['qty'];
                $payload_item[SaleItem::PRICE] = $item->price;
                $payload_item[SaleItem::INCOME] = ($value['qty'] * $item->price) - ($value['qty'] * $item->product->capital_price);
                SaleItem::create($payload_item);
            }

            DB::commit();
            return redirect()->route('sale.index')->with(['status' => 'Penjualan Produk', 'message' => 'berhasil diperbarui.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    public function destroy(Sale $sale) {
        $items = $sale->items;
        foreach ($items as $item) {
            $product = ProductType::find($item->type_id);
            $product->incrementStok($item->qty);
        }

        $sale->delete();

        return response(['status' => true, 'message' => 'berhasil terhapus.']);
    }

    function trash() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) {
                $sales = Sale::with('user','items')->onlyTrashed()->get();
            } else {
                $sales = Sale::with('user','items')->whereHas('user', function ($q) {
                    $q->where(User::STORE_ID, auth()->user()->store_id);
                })->onlyTrashed()->get();
            }
            return response(['data' => $sales]);
        }
        return view('page.sale.trash');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\PurchaseImage;
use App\Models\Store;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PurchaseController extends Controller {

    public function index() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) 
                $purchases = Purchase::with('store','supplier','images')->latest(Purchase::DATE, 'desc')->get();
            else
                $purchases = Purchase::with('store','supplier','images')->where(Purchase::STORE_ID, auth()->user()->store_id)->latest(Purchase::DATE, 'desc')->get();

            return response(['data' => $purchases]);
        }

        if (auth()->guard('admin')->check()) {
            $stores = Store::get();
            $suppliers = Supplier::orderBy(Supplier::FULLNAME)->get();
        } else {
            $stores = auth()->user()->store;
            $suppliers = Supplier::orderBy(Supplier::FULLNAME)->where(Supplier::STORE_ID, $stores->id)->get();
        }

        return view('page.purchase.index', compact('stores','suppliers'));
    }

    public function store(Request $request) {
        // return response($request->all());
        DB::beginTransaction();
        try {
            $supplier = Supplier::find($request->input(Purchase::SUPPLIER_ID));
            $payload = $request->except('image');
            $payload[Purchase::SUPPLIER_NAME] = $supplier->fullname ?? '-';
            $purchase = Purchase::create($payload);
    
            foreach ($request->file('image') as $image) {
                $photo = Storage::disk('public')->put(PurchaseImage::IMAGE_PATH, $image);
                $purchase->images()->create([
                    PurchaseImage::IMAGE => basename($photo)
                ]);
            }

            DB::commit();
            return redirect()->back()->with(['status' => 'Pembelian', 'message' => 'berhasil tersimpan']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    public function show(Purchase $purchase) {
        if (auth()->guard('admin')->check()) {
            $stores = Store::get();
            $suppliers = Supplier::orderBy(Supplier::FULLNAME)->get();
        } else {
            $stores = auth()->user()->store;
            $suppliers = Supplier::orderBy(Supplier::FULLNAME)->where(Supplier::STORE_ID, $stores->id)->get();
        }

        $purchase->load('images');

        return view('page.purchase.detail', compact('purchase','stores','suppliers'));
    }

    public function update(Request $request, Purchase $purchase) {
        $credentials = $request->all();
        if ($request->input(Purchase::SUPPLIER_ID) != 0) {
            $credentials[Purchase::SUPPLIER_NAME] = Supplier::find($request->input(Purchase::SUPPLIER_ID))->fullname;
        }
        $purchase->update($credentials);

        return redirect()->back()->with(['status' => 'Pembelian', 'message' => 'berhasil diperbarui']);
    }

    public function destroy(Purchase $purchase) {
        $purchase->delete();

        return redirect()->back()->with(['status' => 'Pembelian', 'message' => 'berhasil terhapus']);
    }
}

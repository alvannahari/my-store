<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends Controller {

    public function show(Store $store) {
        if (auth()->guard('owner')->check() && (auth()->user()->store_id != $store->id)) {
            return redirect()->route('store.show', auth()->user()->store_id);
        }

        return view('page.store.detail', compact('store'));
    }

    public function update(Request $request, Store $store) {
        $payload = $request->all();

        $store->update($payload);

        return redirect()->back()->with(['status' => 'Data Toko', 'message' => 'berhasil diperbarui']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OwnerController extends Controller {
    
    function index() {
        if (auth()->guard('owner')->check()) 
            return redirect()->route('owner.show', auth()->id());

        if (request()->ajax()) {
            $owners = User::with('store')->where(User::ROLE,  User::ROLE_OWNER)->get();
            return response(['status' => true, 'data' => $owners]);
        }

        return view('page.owner.index');
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME => 'required|alpha_dash|unique:users',
            User::FULLNAME => 'required|string',
            User::PASSWORD => 'required|confirmed|min:6',
            User::RELIGION => 'required',
            User::PLACE_BIRTH => 'required',
            User::DATE_BIRTH => 'required',
            Store::NAME => 'required',
            Store::DESC => 'nullable',
            Store::ADDRESS => 'required',
            Store::MOBILE => 'required',
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'error' => $validator->errors()]);

        DB::beginTransaction();
        try {
            $payload_store = $request->only(Store::NAME, Store::DESC, Store::ADDRESS, Store::MOBILE);
            $payload_store[Store::OWNER] = $request->input(User::FULLNAME);
            $store = Store::create($payload_store);

            $payload = $request->except(Store::NAME, Store::DESC, Store::ADDRESS, Store::MOBILE);
            $payload[User::PASSWORD] = bcrypt($payload[User::PASSWORD]);
            $payload[User::ROLE] = User::ROLE_OWNER;
            $payload[User::STORE_ID] = $store->id;
            User::create($payload);
    
            DB::commit();
            return response(['status' => true, 'message' => 'Data pemilik toko baru berhasil disimpan.']); 
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['status' => false, 'message' => $th->getMessage()]); 
        }
    }

    function show(User $owner) {
        // $owner = User::where(User::ROLE, User::ROLE_OWNER)->find($user);
        // if (auth()->guard())
        // return response($owner);
        return view('page.owner.detail', compact('owner'));
    }

    function update(Request $request, User $owner) {
        $payload = $request->except(User::PASSWORD, 'password_confirmation');

        $rules = [
            User::USERNAME => 'required|alpha_dash|unique:users,username,'.$owner->id,
            User::FULLNAME => 'required|string',
            User::RELIGION => 'required',
            User::PLACE_BIRTH => 'required',
            User::DATE_BIRTH => 'required'
        ];
        
        if ($request->filled('password')) {
            $rules[User::PASSWORD] = 'required|confirmed|min:6';
            $payload[User::PASSWORD] = bcrypt($payload[User::PASSWORD]);
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response(['status' => false,'error' => $validator->errors()]);

        $owner->update($payload);

        session()->flash('status', 'Akun Pemilik');
        session()->flash('message', 'berhasil diperbarui');

        return response(['status' => true]);
    }

    function destroy(User $owner) {
        $owner->delete(); 
        return response(['status' => 'Akun Pemilik', 'message' => 'berhasil terhapus']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class LogActivityController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            if (auth()->guard('owner')->check())  {
                $users_id = auth()->user()->store->users->pluck(User::ID);
                $activities = Activity::with('causer')->whereHas('causer', function($q) use ($users_id) {
                    $q->whereIn(User::ID, $users_id);
                })->get();
            } else {
                $activities = Activity::with('causer')->get();
            }
    
            return response(['data' => $activities]);
        }

        return view('page.log-activity.index');
    }

    function destroy(Activity $activity) {
        $activity->delete();

        return redirect()->back();
    }
}

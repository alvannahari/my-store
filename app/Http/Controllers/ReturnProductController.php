<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ReturnProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReturnProductController extends Controller {

    public function index() {
        if (request()->ajax()) {
            if (auth()->guard('admin')->check()) 
                $data = ReturnProduct::with('user','product')->get();
            else 
                $data = ReturnProduct::with('user','product')->whereHas('user', function($q) {
                    $q->where(ReturnProduct::USER_ID, auth()->id());
                })->get();

            return response(['data' => $data]);
        }
        $products = Product::get();
        return view('page.return-product.index', compact('products'));
    }

    public function store(Request $request) {
        $credentials = $request->all();

        DB::beginTransaction();

        try {
            $product = Product::find($credentials[ReturnProduct::PRODUCT_ID]);
            $product->decrementStok($request->input(ReturnProduct::QTY));

            $credentials[ReturnProduct::NAME] = $product->name;
            $credentials[ReturnProduct::CODE] = $product->code;
    
            ReturnProduct::create($credentials);
    
            DB::commit();
            return redirect()->back()->with(['status' => 'Pengembalian Produk', 'message' => 'data berhasil tersimpan']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    public function show(ReturnProduct $returnProduct) {
        return view('page.return-product.detail', compact('returnProduct'));
    }

    public function update(Request $request, ReturnProduct $returnProduct) {
        $payload = $request->all();

        DB::beginTransaction();

        try {
            if ($payload[ReturnProduct::PRODUCT_ID] != 0) {
                $product = Product::find($payload[ReturnProduct::PRODUCT_ID]);
                if ($returnProduct->qty != $payload[ReturnProduct::QTY]) {
                    $qty = $payload[ReturnProduct::QTY] - $returnProduct->qty;
                    $product->decrementStok($qty);
                }
            }

            $returnProduct->update($payload);
    
            DB::commit();
            return redirect()->route('return-product.index')->with(['status' => 'Pengembalian Produk', 'message' => 'data berhasil tersimpan']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('return-product.index')->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }

    public function destroy(ReturnProduct $returnProduct) {
        DB::beginTransaction();

        try {
            if ($returnProduct[ReturnProduct::PRODUCT_ID] != 0) {
                $product = Product::find($returnProduct->product_id);
                $product->incrementStok($returnProduct->qty);
            }
            $returnProduct->delete();
            
            DB::commit();
            return redirect()->back()->with(['status' => 'Pengembalian Produk', 'message' => 'data berhasil terhapus']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => $th->getMessage()]);
        }
    }
}

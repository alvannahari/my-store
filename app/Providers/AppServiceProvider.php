<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Relation::morphMap([
            'user' => 'App\Models\User',
            'admin' => 'App\Models\Admin',
            'product' => 'App\Models\Product',
            'product_price' => 'App\Models\ProductPrice',
        ]);
    }
}

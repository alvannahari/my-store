<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Admin - My POS Store </title>
    @stack('prepend-style')
    @include('includes.style')
    @stack('addons-style')
</head>

<body>
    <div class="loader"></div>
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar sticky">
            <div class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                <li>
                    <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn"> 
                        <i data-feather="align-justify"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="nav-link nav-link-lg fullscreen-btn">
                        <i data-feather="maximize"></i>
                    </a>
                </li>
                <li style="display: flex;align-items: center;">
                    <div class="search-element">
                        <h6 class="mb-0 mt-1 ml-2">
                            @yield('title')
                        </h6>
                    </div>
                </li>
                </ul>
            </div>
            <ul class="navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <span style="color: #7d858c;font-size:16px" class="mr-2">Hello, {{ auth()->user()->fullname }}</span> 
                        <img alt="image" src="{{ asset('assets/img/user.png') }}" class="user-img-radious-style"> 
                        <span class="d-sm-none d-lg-inline-block"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right pullDown">
                        <div class="dropdown-title">Hello {{ auth()->guard(session()->get('guard'))->user()->fullname }}</div>
                        @auth('owner')
                            <a href="{{ route('owner.store', auth()->id()) }}" class="dropdown-item has-icon"> 
                                <i class="far fa-user"></i> Akun Saya
                            </a> 
                            <a href="{{ route('store.show', auth()->user()->store_id) }}" class="dropdown-item has-icon"> 
                                <i class="fas fa-cog"></i>
                                Toko Saya
                            </a>
                            <a href="{{ route('log-activity.index') }}" class="dropdown-item has-icon"> 
                                <i class="fas fa-bolt"></i> Aktifitas
                            </a> 
                        @endauth
                        <div class="dropdown-divider"></div>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="dropdown-item has-icon text-danger" style="font-weight: 500;line-height: 1.2;font-size: 13px;" type="submit"> 
                                <i class="fas fa-sign-out-alt"></i>
                                Logout
                            </button>
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="main-sidebar sidebar-style-2">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                <a href="{{ route('home') }}"> <img alt="image" src="{{ asset('assets/img/logo.png') }}" class="header-logo" /> <span
                    class="logo-name">STORE</span>
                </a>
                </div>
                <ul class="sidebar-menu">
                    @include('includes.sidebar')
                </ul>
            </aside>
        </div>
        <!-- Main Content -->
        <div class="main-content">
            @yield('content')
        </div>
        <div id="notifications-state">
            @if (Session::get('status'))
                <span data-message="{{ Session::get('message') }}" data-status="{{ Session::get('status') }}"></span>
            @endif
        </div>
        <footer class="main-footer">
            <div class="footer-left">
                <a href="https://www.instagram.com/krakatio/?hl=id" target="_blank">Krakatio @2024</a></a>
            </div>
            <div class="footer-right"></div>
        </footer>
    </div>

    @stack('prepend-script')
    @include('includes.script')
    <script>
        function clearError(form) {
            // if($(that).hasClass('shopping')){
            //     $(that).removeClass('shopping');
            //     alert( $(that).attr('class'));//btn btn-warning
            // }
            $('#'+form).find('.is-invalid').next().remove();
            $('#'+form).find('.is-invalid').removeClass('is-invalid');
            // .removeClass('is-invalid').next().remove();
            // $('#'+form+' textarea').removeClass('is-invalid').next().remove();
        }
    </script>
    @stack('addons-script')
</body>
<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>
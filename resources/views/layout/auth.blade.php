<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Admin - My POS Store </title>
    @stack('prepend-style')
    @include('includes.style')
    @stack('addons-style')
</head>

<body>
    <div class="loader"></div>
    <section class="section">
        <div class="container mt-5">
            @yield('content')
        </div>
    </section>
    @stack('prepend-script')
    @include('includes.script')
    @stack('addons-script')
</body>
<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>
@extends('layout.main')

@section('title', 'Detail Pemilik')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form id="form-owner">
                    <div class="card-body pb-1">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control" required value="{{ $owner->username }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="fullname" class="form-control" required value="{{ $owner->fullname }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="-- kosongi jika tidak ingin mengubah password --">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="-- kosongi jika tidak ingin mengubah password --">
                                </div>
                            </div>
                        </div>
                        
                    <div class="form-group mb-3">
                        <label>Jenis Kelamin</label>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-check">
                                    <input class="form-check-input" value="Pria" type="radio" name="gender" id="radio-gender-men" {{ $owner->gender == 'Pria' ? 'checked' : '' }}> 
                                    <label class="form-check-label" for="radio-gender-men">
                                        Pria
                                    </label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-check">
                                    <input class="form-check-input" value="Wanita" type="radio" name="gender" id="radio-gender-women" {{ $owner->gender == 'Wanita' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="radio-gender-women">
                                        Wanita
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" name="place_birth" required value="{{ $owner->place_birth }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="date" class="form-control" name="date_birth" required value="{{ $owner->date_birth }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Agama</label>
                                <input type="text" class="form-control" name="religion" required value="{{ $owner->religion }}">
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary" id="btn-update-user"><i class="fas fa-plus"></i> Simpan Data Kasir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@push('addons-script')
<script>
    $('#btn-update-user').click(function (e) { 
        e.preventDefault();
        clearError('form-owner');
        let btn = $(this);

        $.ajax({
            type: "post",
            url: "{{ route('owner.update', $owner->id) }}",
            data: $('#form-owner').serialize(),
            dataType: "json",
            beforeSend : function () {
                btn.addClass('btn-progress');
            },
            success: function (response) {
                if (response.status) {
                    location.reload()
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').after('<div class="invalid-feedback">'+response.error[key]+'.</div>');
                    }
                    alert('Something wrong !!!');
                    btn.removeClass('btn-progress');
                }
            },
            error: function(xhr, status, error) {
                btn.removeClass('btn-progress');
                console.log(xhr.responseText);
            }
        });
    });
</script>
@endpush
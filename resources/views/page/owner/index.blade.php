@extends('layout.main')

@section('title', 'Manajemen Akun Pemilik')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-owner" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Email</th>
                                    <th>Nama Lengkap</th>
                                    <th>JK</th>
                                    <th>TTL</th>
                                    <th>Agama</th>
                                    <th>Toko</th>
                                    <th>Alamat</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Owner-->
<div class="modal fade" id="modal-add-owner" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pemilik dan Toko Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" id="form-add-owner">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="fullname" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label class="mb-3 pb-1">Jenis Kelamin</label>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" value="Pria" type="radio" name="gender" id="radio-gender-men" checked> 
                                            <label class="form-check-label" for="radio-gender-men">
                                                Pria
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" value="Wanita" type="radio" name="gender" id="radio-gender-women" >
                                            <label class="form-check-label" for="radio-gender-women">
                                                Wanita
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Agama</label>
                                <input type="text" class="form-control" name="religion" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" name="place_birth" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control" name="date_birth" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Nama Toko</label>
                                <input type="text" class="form-control" name="name" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Alamat Toko</label>
                                <input type="text" class="form-control" name="address" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input type="text" class="form-control" name="desc">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>No. Telp/WA</label>
                                <input type="text" class="form-control" name="mobile_number" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-add-owner">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus akun kasir ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-owner">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    var owner_id = 0;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"}
        })
        
        var dt_owners = $('#table-owner').DataTable( {
            processing: true,
            ajax: "{!! url()->current() !!}",
            order: [[2, 'asc']],
            columns: [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "username"},
                { "data": "fullname"},
                { "data": "gender", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return row.place_birth+', '+row.date_birth
                    }
                },
                { "data": "religion", "sClass": "text-center" },
                { "data": "store.name"},
                { "data": "store.address"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('owner.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-sm btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-sm btn-danger" onclick="deleteOwner(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            initComplete: function(){
                $("#table-owner_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-success mb-2" data-toggle="modal" data-target="#modal-add-owner"><i class="fa fa-plus mr-2"></i> Tambah Pemilik Toko Baru</button>
                    </div>
                `);
                $("#table-owner_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
                $("#table-owner_length").css('text-align', 'center');
                $("#table-owner_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
            }   
        });
        dt_owners.on( 'order.dt search.dt', function () {
            dt_owners.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-add-owner').click(function (e) { 
            e.preventDefault();
            let btn = $(this);

            $.ajax({
                type: "post",
                url: "{{ route('owner.store') }}",
                data: $('#form-add-owner').serialize(),
                dataType: "json",
                beforeSend : function () {
                    btn.addClass('btn-progress');
                },
                success: function (response) {
                    if (response.status) {
                        dt_owners.ajax.reload(function () {
                            $('#modal-add-owner').modal('hide');
                            showAlert('success',response.message,'Pemilik Toko Baru')
                            clearForm();
                        }, false);
                    } else {
                        showAlert('Error', 'Silakan periksa formulir', 'Pemilik Toko Baru')
                        for (let key of Object.keys(response.error)) {
                            $('#form-add-owner [name="'+key+'"]').addClass('is-invalid');
                            $('#form-add-owner [name="'+key+'"]').next().html(response.errors[key]);
                        }
                    }
                    btn.removeClass('btn-progress');
                },
                error: function(xhr, status, error) {
                    showAlert('error',XMLHttpRequest.responseJSON.message,'Pemilik Toko Baru')
                    console.log(XMLHttpRequest.responseJSON.message);
                    btn.removeClass('btn-progress')
                }
            });
        });

        $('#btn-delete-owner').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('owner.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', owner_id),
                success: function (response) {
                    if (response.status) {
                        dt_owners.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Hapus Pemilik Toko')
                        }, false);
                    }
                },
            });
        });
    });

    function deleteOwner(id) {
        owner_id = id;
        $('#modal-delete').modal('show');
    }
</script>
@endpush


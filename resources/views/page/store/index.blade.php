@extends('layout.main')

@section('title', 'Daftar Store')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                    <h4>Basic DataTables</h4>
                </div> --}}
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-store" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama</th>
                                    <th>Pemilik</th>
                                    <th>Alamat</th>
                                    <th>No. Hp</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr>
                                    <td>1</td>
                                    <td>Create a mobile app</td>
                                    <td class="align-middle">
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-success width-per-40">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <img alt="image" src="assets/img/users/user-5.png" width="35">
                                    </td>
                                    <td>2018-01-20</td>
                                    <td><div class="badge badge-success badge-shadow">Completed</div></td>
                                    <td><a href="#" class="btn btn-primary">Detail</a></td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="modal-add-store" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Toko Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" method="POST" action="{{ route('store.store') }}" >
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Nama Toko</label>
                        <input type="text" class="form-control" placeholder="nama toko" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Pemilik</label>
                        <input type="text" class="form-control" placeholder="pemilik toko" name="owner" required>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="desc" rows="5" class="form-control" placeholder="deskripsi singkat tentang toko ini" style="height: auto !important"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" class="form-control" placeholder="alamat toko" name="address">
                    </div>
                    <div class="form-group">
                        <label>No. Handphone</label>
                        <input type="text" class="form-control" name="mobile_number" placeholder="+62 8576 3194 806">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('store.delete',1) }}" method="POST" id="form-delete">
                @csrf
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus toko store ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>
<script>
    $(document).ready(function () {
        let t = $('#table-store').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name"},
                { "data": "owner"},
                { "data": "address"},
                { "data": "mobile_number"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('store.detail', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-danger" onclick="deleteStore(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-store_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-success mb-2" type="button" data-toggle="modal" data-target="#modal-add-store"><i class="fa fa-plus"></i> Tambah store</button>
                    </div>
                `);
                $("#table-store_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-store_length").css('text-align', 'center');
                $("#table-store_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }   
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deleteStore(id) {
        let url_delete = "{{ route('store.delete', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }
</script>
@endpush


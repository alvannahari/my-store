@extends('layout.main')

@section('title', 'Informasi Toko')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('store.update', $store->id) }}" id="form-store" method="POST">
                    <div class="card-body pb-1">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama Toko</label>
                                    <input type="text" class="form-control" name="name" required value="{{ $store->name }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Pemilik</label>
                                    <input type="text" class="form-control" name="owner" required value="{{ $store->owner }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label>Deskripsi Toko</label>
                                    <textarea name="desc" rows="4" class="form-control" style="height: auto !important">{{ $store->desc }}</textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label>Alamat</label> 
                                    <textarea type="text" rows="4" class="form-control" style="height: auto !important"> {{ $store->address }} </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>No. Handphone</label> 
                                    <input type="text" class="form-control" value="{{ $store->mobile_number }}" name="mobile_number">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Terakhir Diperbarui</label> 
                                    <input type="text" class="form-control" value="{{ $store->updated_at }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Simpan Perubahan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
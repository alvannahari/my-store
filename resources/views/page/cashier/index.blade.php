@extends('layout.main')

@section('title', 'Manajemen Akun Kasir')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-cashier" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Email</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jenis Kelamin</th>
                                    <th>TTL</th>
                                    <th>Agama</th>
                                    <th>Toko</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Cashier-->
<div class="modal fade" id="modal-add-cashier" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kasir Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" id="form-cashier">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="fullname" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label>Jenis Kelamin</label>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-check">
                                    <input class="form-check-input" value="Pria" type="radio" name="gender" id="radio-gender-men" checked> 
                                    <label class="form-check-label" for="radio-gender-men">
                                        Pria
                                    </label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-check">
                                    <input class="form-check-input" value="Wanita" type="radio" name="gender" id="radio-gender-women" >
                                    <label class="form-check-label" for="radio-gender-women">
                                        Wanita
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" name="place_birth" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="date" class="form-control" name="date_birth" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Agama</label>
                                <input type="text" class="form-control" name="religion" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label>Nama Toko</label>
                            @auth('admin')
                                <select name="store_id" class="form-control">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" class="form-control" name="store_id" value="{{ $stores->id }}">
                                <input type="text" class="form-control" value="{{ $stores->name }}" disabled>
                            @endauth
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-add-user">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('cashier.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus akun kasir ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"}
        })
        
        let t = $('#table-cashier').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "username"},
                { "data": "fullname"},
                { "data": "gender", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return row.place_birth+', '+row.date_birth
                    }
                },
                { "data": "religion", "sClass": "text-center" },
                { "data": "store.name"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('cashier.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-sm btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-sm btn-danger" onclick="deletecashier(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-cashier_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-success mb-2" data-toggle="modal" data-target="#modal-add-cashier"><i class="fa fa-plus mr-2"></i> Tambah Kasir</button>
                    </div>
                `);
                $("#table-cashier_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
                $("#table-cashier_length").css('text-align', 'center');
                $("#table-cashier_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
            }   
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deletecashier(id) {
        let url_delete = "{{ route('cashier.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }

    $('#btn-add-user').click(function (e) { 
        e.preventDefault();
        clearError('form-cashier');
        let btn = $(this);

        $.ajax({
            type: "post",
            url: "{{ route('cashier.store') }}",
            data: $('#form-cashier').serialize(),
            dataType: "json",
            beforeSend : function () {
                btn.addClass('btn-progress');
            },
            success: function (response) {
                if (response.status) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').after('<div class="invalid-feedback">'+response.error[key]+'.</div>');
                    }
                    showAlert('Error', 'Silakan periksa formulir', 'Kasir')
                    btn.removeClass('btn-progress');
                }
            },
            error: function(xhr, status, error) {
                btn.removeClass('btn-progress');
                showAlert('Error', 'Terjadi Kesalahan', 'Kasir')
                console.log(xhr.responseText);
            }
        });
    });
</script>
@endpush


@extends('layout.main')

@section('title', 'Daftar Pengembalian Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-product" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Product-->
<div class="modal fade" id="modal-add-return" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Pengembalian Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" method="POST" action="{{ route('return-product.store') }}" >
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                    <div class="form-group">
                        <label>Produk yang dikembalikan</label>
                        <select name="product_id" class="form-control">
                            @foreach ($products as $product)
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Banyak Barang</label>
                        <input type="number" class="form-control" name="qty">
                    </div>
                    <div class="form-group">
                        <label>Note.</label>
                        <textarea name="note" rows="5" class="form-control" placeholder="catatan pengembalian produk"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('return-product.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE" />
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus data pengembalian barang ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>
<script>
    $(document).ready(function () {
        let t = $('#table-product').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name"},
                { "data": "code"},
                { "data": "qty", "sClass": "text-center"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (row.status == 'pending')
                            return `<label class="badge badge-warning">Pending</label>`; 
                        else
                            return `<label class="badge badge-success">Succeed</label>`; 
                    }
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('return-product.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-danger" onclick="deleteProduct(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-product_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-success mb-2" type="button" data-toggle="modal" data-target="#modal-add-return"><i class="fa fa-plus"></i> Tambahkan Pengembalian</button>
                    </div>
                `);
                $("#table-product_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-product_length").css('text-align', 'center');
                $("#table-product_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }   
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deleteProduct(id) {
        let url_delete = "{{ route('return-product.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }
</script>
@endpush


@extends('layout.main')

@section('title', 'Detail Pengembalian Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('return-product.update', $returnProduct->id) }}" id="form-product" method="POST">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="product_id" value="{{ $returnProduct->product_id }}">
                    <div class="card-body pb-1">
                        @csrf
                        <div class="form-group">
                            <label>Nama Product</label>
                            <input type="text" class="form-control" name="name" required value="{{ $returnProduct->name }}">
                        </div>
                        <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" name="code" required value="{{ $returnProduct->code }}">
                        </div>
                        <div class="form-group">
                            <label>Banyak Barang</label>
                            <input type="text" class="form-control" name="qty" required value="{{ $returnProduct->qty }}">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control">
                                <option value="pending" {{ $returnProduct->status == 'pending' ? 'selected' : '' }}>Pending</option>
                                <option value="succeed" {{ $returnProduct->status == 'succeed' ? 'selected' : '' }}>Succeed</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Note.</label>
                            <textarea name="note" rows="5" class="form-control" style="height: auto !important">{{ $returnProduct->note }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-pencil-alt"></i> Simpan Perubahan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
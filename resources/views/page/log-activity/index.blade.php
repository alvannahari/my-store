@extends('layout.main')

@section('title','Riwayat Aktivitas Admin')

@section('content')
<section class="section">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table-log" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>User</th>
                            <th>Activity</th>
                            <th>ID Model</th>
                            <th>Data Lama</th>
                            <th>Data Baru</th>
                            <th>Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('category.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus kategori ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>
<script>
    $(document).ready(function () {
        let t = $('#table-log').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "pageLength": 100,
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "causer.fullname"},
                { "data": "description"},
                { "data": "subject_id"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (row.properties.old)
                            return JSON.stringify(row.properties.old, null, 4);
                        else return '-';
                    },"bSortable": false
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (row.properties.attributes)
                            return JSON.stringify(row.properties.attributes, null, 4);
                        else return '-';
                    },"bSortable": false
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let date = new Date(row.created_at);
                        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
                    }
                },
                // { "data": "created_at", "sClass": "text-center" },
                // { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                //         return `
                //             <button class="btn btn-danger" onclick="deleteLog(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                //             `;
                //     },"bSortable": false
                // }
            ]
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    // function deleteLog(id) {
    //     let url_delete = "{{ route('category.destroy', ':id') }}";
    //     $('#form-delete').attr('action', url_delete.replace(':id', id));
    //     $('#modal-delete').modal('show');
    // }
</script>
@endpush
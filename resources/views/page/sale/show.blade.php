@extends('layout.main')

@section('title', 'Detail Penjualan Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-lg-9 col-xl-8">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped pr-4" id="table-sale">
                                    <thead>
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Nama Produk</th>
                                            <th scope="col">Kode</th>
                                            <th scope="col" class="text-center">Banyak Item</th>
                                            <th scope="col" class="text-right">Harga</th>
                                            <th scope="col" class="text-right pr-5">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sale->items as $item)
                                            <tr>
                                                <td scope="row">{{ $loop->iteration }}</td>
                                                <td class="align-middle">{{ $item->name }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td class="text-center" style="width: 120px">{{ $item->qty }} x</td>
                                                <td class="text-right">Rp. {{ number_format($item->price,0,'','.') }}</td>
                                                {{-- <td class="text-center" >
                                                    <input type="hidden" name="item[{{ $loop->index }}][id]" value="{{ $item->id }}" form="form-sale" />
                                                    <input class="input-qty-item" type="number" value="{{ $item->qty }}" min="1" name="item[{{ $loop->index }}][qty]" max="1000" form="form-sale"/>
                                                </td> --}}
                                                <td class="text-right pr-5">Rp. {{ number_format($item->qty * $item->price,0,'','.') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3 col-xl-4">
                            <div class="py-4 pl-2 mb-2" style="color: white; background-color:#353c48 ">
                                <h3>Total <span class="mr-3">:</span> Rp. {{ number_format($sale->final_amount,0,'','.') }}</h3>
                            </div>
                            <table class="table">
                                <tbody class="font-weight-bold" style="font-size: 14px;">
                                    <tr>
                                        <td>Nama Pembeli</td>
                                        <td class="text-center">:</td>
                                        <td>{{ $sale->cust_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Kasir</td>
                                        <td class="text-center">:</td>
                                        <td>{{ $sale->user->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 160px;">Metode Pembayaran</td>
                                        <td class="text-center">:</td>
                                        <td>{{ $sale->type_payment }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td class="text-center">:</td>
                                        <td>Rp. {{ number_format($sale->total_price, 0,'','.') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Diskon</td>
                                        <td class="text-center">:</td>
                                        <td>- Rp. {{ number_format($sale->discount, 0,'','.') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td class="text-center">:</td>
                                        <td>Rp. {{ number_format($sale->final_amount, 0,'','.') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Uang Pembayaran</td>
                                        <td class="text-center">:</td>
                                        <td>Rp. {{ number_format($sale->paid_amount, 0,'','.') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Uang Kembalian</td>
                                        <td class="text-center">:</td>
                                        <td>Rp. {{ number_format($sale->change, 0,'','.') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Catatan</td>
                                        <td class="text-center">:</td>
                                        <td>{{ $sale->note }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mb-2">
                                {{-- <div class="col-12 px-4">
                                    <button class="btn btn-warning mb-3 w-100" type="button" id="btn-save-sale"><i class="fas fa-undo mr-2"></i> Update Transaksi</button>
                                </div> --}}
                                <div class="col-12 px-4">
                                    <button class="btn btn-info w-100" type="button" id="btn-print-sale" data-id="{{ $sale->id }}"><i class="fa fa-print mr-2"></i> Cetak Transaksi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('addons-script')
<script src="{{ asset('assets/bundles/input-spinner/bootstrap-input-spinner.js') }}"></script>
<script>
    // $(".input-qty-item").inputSpinner();
    $('#btn-print-sale').click(function (e) {
        let sale_id = $(this).attr("data-id");
        let url_print = "{{ route('transaction.show', ':id') }}";

        if (sale_id == 0) {
            alert('Transaksi tidak ditemukan.');
            return false;
        }

        $.ajax({
            type: "GET",
            url: url_print.replace(':id', sale_id),
            dataType: "json",
            success: function (response) {
                if (response.status)
                    popupCenter({url: url_print.replace(':id', sale_id), title: 'xtf', w: 700, h: 440}); 
                else 
                    alert('Transaksi tidak ditemukan.')
            }
        });
    });
</script>
@endpush
@extends('layout.main')

@section('title', 'Riwayat Penjualan')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-sale" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Pembeli</th>
                                    <th>Subtotal</th>
                                    <th>Total Harga & ID</th>
                                    <th>Uang Pembayaran</th>
                                    <th>Metode</th>
                                    <th>Status</th>
                                    <th>Produk</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus Riwayat Transaksi ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger" id="btn-delete-sale">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    var sale_id = 1;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"}
        })

        let dt_sales = $('#table-sale').DataTable( {
            processing: true,
            ajax: "{!! url()->current() !!}",
            order : [[8, 'desc']],
            columns: [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", render: function ( data, type, row ) {
                        return '<p class="mb-0">'+row.cust_name+'</p><small>'+row.user.fullname+'</small>'
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<p class="mb-1">'+formatRupiah(row.total_price)+'</p><small>- '+formatRupiah(row.discount)+'</small>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<h6 class="mb-0"><b>'+formatRupiah((row.final_amount))+'</b></h6><small>'+row.id+'</small>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<p class="mb-0">'+formatRupiah(row.paid_amount)+'</p><small> '+formatRupiah(row.change)+'</small>';
                    }
                },
                { "data": "type_payment"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return row.is_completed != 0 ? '<span class="badge badge-sm badge-success"> Selesai </span>' : '<span class="badge badge-sm badge-secondary"> Pending </span>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        let html = '<small>';
                        $.each(row.items, function (index, value) { 
                            html += value.qty+' x '+value.name+' @ '+value.price+'<br>'
                        });
                        return html += '</small>';
                    }
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('sale.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-sm btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-sm btn-danger" onclick="deleteSale('`+row.id+`')"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-sale_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
                $("#table-sale_filter").addClass('text-xl-center');
                $("#table-sale_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
            }
        });
        dt_sales.on( 'order.dt search.dt', function () {
            dt_sales.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-delete-sale').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('sale.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', sale_id),
                success: function (response) {
                    if (response.status) {
                        dt_sales.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Berhasil')
                        }, false);
                    }
                },
            });
        });
    });

    function deleteSale(id) {
        sale_id = id;
        $('#modal-delete').modal('show');
    }
</script>
@endpush


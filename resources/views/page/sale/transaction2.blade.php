@extends('layout.main')

@section('title', 'Transaksi Baru')

@section('content')
<section class="section">
    <div class="card">
        {{-- <div class="card-header">
            <h4>Vertical Layout</h4>
        </div> --}}
        <div class="card-body">
            <form id="wizard_with_validation" method="POST">
                <h3>Pemilihan Produk</h3>
                <fieldset>
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Pencarian Produk/Kode" id="search" style="height: 36px;">
                        <div class="input-group-btn">
                            <button class="btn btn-primary btn-icon"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="input-group-btn ml-3">
                            <button class="btn btn-sm btn-warning btn-icon" data-toggle="modal" data-target="#modal-scan" style="line-height: 31px;">
                                <i class="material-icons mr-2" style="vertical-align: middle;">refresh</i> 
                                Refresh Data
                            </button>
                        </div>
                        <div class="input-group-btn ml-3">
                            <button class="btn btn-sm btn-danger btn-icon" data-toggle="modal" data-target="#modal-scan" style="line-height: 31px;">
                                <i class="material-icons mr-2" style="vertical-align: middle;">delete</i> 
                                Reset Data
                            </button>
                        </div>
                    </div>
                    <div class="row mt-4 wrapper-product" >
                    </div>
                </fieldset>
                <h3>Informasi Pembayaran</h3>
                <fieldset>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <label class="form-label">First Name*</label>
                        <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <label class="form-label">Last Name*</label>
                        <input type="text" name="surname" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <label class="form-label">Email*</label>
                        <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <label class="form-label">Address*</label>
                        <textarea name="address" cols="30" rows="3" class="form-control no-resize"
                            required></textarea>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <label class="form-label">Age*</label>
                        <input min="18" type="number" name="age" class="form-control" required>
                        </div>
                        <div class="help-info">The warning step will show up if age is less than 18</div>
                    </div>
                </fieldset>
                <h3>Nota Pembayaran dan Selesai</h3>
                <fieldset>
                    <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                    <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                </fieldset>
            </form>
        </div>
    </div>
</section>

@endsection

@push('addons-style')
<style>
    #card-products {
        height: 965px !important;
        overflow: hidden;
    }
    #card-transaction {
        height: auto;
        overflow: hidden;
    }
    .wrapper-product {
        height:820px;
        overflow-y: auto;
        align-content: flex-start;
    }
    .wrapper-product .loading {
        background-image: url('assets/img/typing.svg');
        background-repeat: no-repeat;
        text-align: center;
        background-size: 65px;
        background-position: center;
        padding-bottom: 45px;
    }
    .list-product {
        height: fit-content;
    }
    .item-product {
        border: 2px solid #d3d3d3;
        border-radius: 10px;
        box-shadow: 0px 1px 1px 1px #d3d3d3;
        overflow: hidden;
        padding-bottom: 10px;
        margin-bottom: 14px;
    }
    .item-product:hover {
        cursor: pointer;
        box-shadow: 0px 0px 12px 3px #d3d3d3;
        /* filter: drop-shadow(0px 2px 6px #868686); */
    }
    .item-product>img {
        max-width: 100%;
        max-height: 118px;
    }
    .product-selected {
        position: absolute;
        left: 50%;
        top: 52%;
        transform: translate(-50%, -55%);
        width: 85%;
        height: calc(100% - 11px);
        background-color: #595a5b59;
        border-radius: 10px;
    }
    .product-selected:hover {
        cursor: default;
        box-shadow: none;
    }
    .product-selected>i {
        color: green;
        font-size: 50px;
        padding-top: 50px;
    }
    .card p {
        padding: 0px 3px;
        margin-top: 3px;
        margin-bottom: 0px;
        /* white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis; */
        /* color: #5b5d5f !important; */
        line-height: 18px;
    }
    .form-group{
        margin-bottom: 12px;
    }
    .table thead th {
        padding: 10px;
        top: 0px;
        position: sticky;
        z-index: 2;
        background-color: #ffffff;
        box-shadow: inset 0px -1px 1px 0px #a7a7a7;
    }
    .table td, .table:not(.table-bordered) th {
        border-top: none;
        vertical-align: middle;
    }
    button.btn-outline-secondary {
        min-width: 2.2rem !important;
    }
    input[inputmode="decimal"] {
        height: 38px !important;
    }

    #table-receipt tbody td:not(:nth-child(2)){
        text-wrap: nowrap;
    }
</style>
@endpush

@push('addons-script')
<script src="{{ asset('assets/bundles/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/input-spinner/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-steps/jquery.steps.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/form-wizard.js') }}"></script>
<script>
    $(".input-qty-item").inputSpinner();

    $(document).ready(function () {
        loadElListProducts();

        $('#modal-add-item').on('shown.bs.modal', function (e) {
            items.columns.adjust();
        });

        $('#form-sale input').keyup(function (e) { 
            $(this).removeClass('is-invalid');
            $(this).next().remove();
        });
    });

    var arr_item_id = [];
    var index_item = 0;
    var image_not_found = "{{ asset('assets/img/not_found.png') }}";

    function loadElListProducts() {
        $.ajax({
            type: "get",
            url: "{!! url()->current() !!}",
            data : {
                'search' : $('#search').val().length > 1 ? $('#search').val() : ''
            },
            dataType: "json",
            beforeSend : function() {
                $('.wrapper-product').html(`<div class="col-12 my-1 text-center loading text-muted"><p> pencarian produk .... </p></div>`)
            },
            success: function (response) {
                let html = '';
                if (response.data.length === 0) {
                    html += '<div class="col-12 my-1 text-center"><h6>Produk Tidak Ditemukan.</h6></div>'
                } else {
                    response.data.forEach(val => {
                        html += `
                            <div class="col-6 col-lg-4 col-xl-3 text-center my-1 list-product">
                                <div class="item-product `+(arr_item_id.includes(val.id) ? 'selected' : '')+`" data-id="`+val.id+`">
                                    <input type="hidden" name="code" value="`+val.code+`">
                                    <input type="hidden" name="price" value="`+val.price+`">
                                    <img src="`+val.image+`" alt="`+val.name+`" srcset="" >
                                    <p class="pt-2" style="font-weight: 700 !important;">`+val.name+`</p>
                                    <p>Stok : `+val.inventory+`</p>`+
                                    (arr_item_id.includes(val.id) ? '<div class="product-selected"><i class="fas fa-check"></i></div>' : '')
                                +`</div>
                            </div>
                        `
                    });
                }
                $('.wrapper-product').html(html)
            }
        });
    }

    function toggleElProductSelected(id, active) {
        let el = $('.item-product[data-id="'+id+'"]');
        el.addClass('selected');
        // el.toggleClass('selected');

        if(!el.find('.product-selected').length && active)
            el.append(`<div class="product-selected"><i class="fas fa-check"></i></div>`)
        
        if (!active) 
            el.find('.product-selected').remove();

        // if($(el).find('.product-selected').length) { 
        //     $(el).find('.product-selected').remove();
        // } else {
        //     $(el).append(`
        //         <div class="product-selected">
        //             <i class="fas fa-check"></i>
        //         </div>
        //     `)
        // }
    }

    $('.wrapper-product').on('click','.item-product', function (e) { 
        e.preventDefault();

        let id = $(this).data("id");
        if (arr_item_id.indexOf(id) == '-1') {
            let name = $(this).children("p:first").html();
            let code = $(this).children("input:first").val();
            let price = $(this).children("input:last").val();
            let inventory = $(this).children("p:last").html().split(' ').pop();

            if (!insertProductToCartElement(id, name, code, price, inventory))
                alert('Barang sedang tidak tersedia.')

            if (inventory != '0')
                display_product(name, price);
        }
    });

    var timeout;
    $("#search").on("keyup paste", function() {
        let value = $(this).val().toUpperCase();
        if(timeout) clearTimeout(timeout);
        timeout = setTimeout(function() { 
            loadElListProducts() 
        }, 500);
    });

    function searchProduct(value) {
        let $rows = $(".wrapper-product .list-product");

        if(value === ''){
            $rows.show(500);
            return false;
        }

        let search = []

        if (value.indexOf(' ') > -1) {
            search = value.split(' ')
        } else {
            search[0] = value
        }

        $rows.each(function(index) {
            $row = $(this);

            let column1 = $row.find("p:first").html().toUpperCase();
            let column2 = $row.find("p:last").html().toUpperCase();
            let found = false;

            search.forEach(key => {
                if ((column1.indexOf(key) > -1)) {
                    found = true; return false;
                }
                if ((column2.indexOf(key) > -1)) {
                    found = true; return false;
                }
            });
            if (found)
                $row.show(500);
            else
                $row.hide(500);
        });
    }

    $('#table-receipt tbody').on('change','.input-qty-item', function (e) { 
        e.preventDefault();
        let qty = $(this).val();
        let price = $(this).parent().prev().html().split(' ').pop();
        let result = qty*price;
        $(this).parent().next().html('Rp. '+result);
        newTotalPrice(); 
    });

    $('#table-receipt tbody').on('click','.item-delete', function (e) { 
        e.preventDefault()
        let id = $(this).data("id");
        
        arr_item_id = jQuery.grep(arr_item_id, function(value) {
            return value != id;
        });
        $(this).parent().parent().remove();
        refreshItem();
        toggleElProductSelected(id, false);
    });

    function refreshItem() {
        numberingItem();
        newTotalPrice();
    }

    function numberingItem() {
        let count = 1;
        $('#table-receipt tbody tr').each(function() {
            $(this).find("td:first").html(count);
            count++;
        });
    };

    function newTotalPrice() {
        let amount = 0
        $('#table-receipt tbody tr').each(function() {
            amount += ~~$(this).find('td').eq(5).html().split(' ').pop();
        });
        $('#form-sale [name="total_price"]').val(amount);
        let discount = ~~$('#form-sale [name="discount"]').val();
        let total = amount - discount;
        $('h3').html('Total <span class="mr-3">:</span>'+formatRupiah(total));
        $('#form-sale [name="final_amount"]').val(total);

        let type_payment = $('#form-sale select[name="type_payment_id"] option:selected').text()
        if (type_payment != 'Tunai')
            $('#form-sale input[name=paid_amount]').val(total)

        let paid_amount = $('#form-sale [name="paid_amount"]').val();
        if (paid_amount != 0) {
            $('#form-sale [name="change"]').val(paid_amount - total);
        } else {
            $('#form-sale [name="change"]').val(0);
        }
    }

    $('#form-sale').on('change keyup', '[name="discount"],[name="paid_amount"],select[name="type_payment_id"]', function () {
        newTotalPrice();
    })

    $('#btn-save-sale').click(function (e) { 
        e.preventDefault();
        let btn = $(this);

        $.ajax({
            type: "POST",
            url: "{{ route('transaction.store') }}",
            data: $('#form-sale').serialize(),
            dataType: "json",
            beforeSend: function() {
                btn.addClass('btn-progress');
            },
            success: function (response) {
                if (response.status) {
                    showAlert('success', response.message, 'Transaksi Produk');
                    $("#modal-confirm-transaction").modal("hide")
                    printReceipt(response.data.id)
                    $('.wrapper-product').html('');
                    loadElListProducts();
                    resetForm();
                } else {
                    showAlert('error', response.message, 'Transaksi Gagal')
                    for (let key of Object.keys(response.errors)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').after('<div class="invalid-feedback">'+response.errors[key]+'.</div>');
                    }
                }
                btn.removeClass('btn-progress');
            }, 
            error: function(xhr, status, error) {
                btn.removeClass('btn-progress');
                showAlert('error', 'Terjadi Kesalahan', 'Transaksi Produk')
                console.log(xhr.responseText);
            }
        });
    })

    $('#btn-add-sale').click(function (e) { 
        e.preventDefault();

        if (arr_item_id.length == 0) {
            showAlert('error', 'Pilih produk yang dibeli', 'Transaksi Gagal');
            return false
        }

        clearError('form-sale');
        $("#modal-confirm-transaction").modal("show")
    });

    function printReceipt(sale_id) {
        let url_print = "{{ route('transaction.show', ':id') }}";
        
        $.ajax({
            type: "GET",
            url: url_print.replace(':id', sale_id),
            dataType: "json",
            success: function (response) {
                if (response.status)
                    popupCenter({url: url_print.replace(':id', sale_id), title: 'xtf', w: 700, h: 440}); 
                else 
                    alert('Transaksi tidak ditemukan.')
            }
        });
    }

    function resetForm() {
        $('#btn-print-sale').attr('data-id',0);
        $('#form-sale input:not([name="_token"])').val('');
        $('#form-sale tbody').html(``);
        $('.product-selected').remove();
    
        arr_item_id = [];
        index_item = 0;
    
        refreshItem();
    }

    $('#modal-scan').on('shown.bs.modal', function (e) {
        $(this).find('input').focus()
        $('#search').val('')
        loadElListProducts() 
        // let search = $('#search').val()
        // if (search != '') {
        // }
    })

    $('#modal-scan .modal-body').click(function (e) { 
        e.preventDefault();
        $(this).find('input').focus()
    });

    $('#input-scan').change(function (e) { 
        e.preventDefault();
        let code = $(this).val();
        let $rows = $(".wrapper-product .list-product");

        if(code === ''){
            $rows.show(500);
            return false;
        }

        let is_found = false;
        let html = '';

        let id = '';
        let name = '';
        let price = '';
        let inventory = '';
        
        $rows.each(function(index) {
            $row = $(this).children('.item-product');

            var column = $row.children("input[name='code']").val();
            console.log(column);

            if ((column.indexOf(code) > -1)) {
                is_found = true
                id = $row.attr('data-id');
                name = $row.children("p:first").html();
                price = $row.children("input[name='price']").val();
                inventory = $row.children("p:last").html();

                html += `
                    <img src="`+$row.children("img").attr('src')+`" alt="" srcset="" style="max-height:200px">
                    <h6 class="pt-2" style="font-weight: 700 !important;">`+name+`</h6>
                    <p>Kode : `+code+`</p>
                    <p>Price : Rp. `+price+`</p>
                    <p>`+inventory+`</p>
                `
                return false;
                
            } 
        });

        
        if (is_found) {
            if (arr_item_id.indexOf(id) == '-1') {
                if (insertProductToCartElement (id, name, code, price, inventory.split(' ').pop())){
                    display_product(name, price);
                    html += '<br><br><h6 class="text-success">Produk berhasil di pindai dan ditambahkan. !</h6>'
                } else {
                    html += '<br><br><h6 class="text-warning">Produk berhasil di pindai tetapi barang tidak tersedia. !</h6>'
                }
            } else {
                addQty(id)
                html += '<br><br><h6 class="text-success">Produk berhasil di pindai dan barang dalam keranjang ditambahkan. !</h6>'
                // html += '<br><br><h6 class="text-warning">Produk berhasil di pindai tetapi sudah terdapat dalam keranjang. !</h6>'
            }
            $('#modal-scan #result-scan').html(html).hide().fadeIn();
        } else {
            html = `
                    <img src="`+image_not_found+`" alt="" srcset="" style="max-height:200px">
                    <h6 class="pt-2" style="font-weight: 700 !important;">Nama Produk : -</h6>
                    <p>Kode : `+code+`</p>
                    <p>Price : Rp. -</p>
                    <p> Stok : -</p>
                    <br><br><h6 class="text-warning">Item berhasil di pindai tetapi produk tidak ditemukan. !</h6>
                `

            $('#modal-scan #result-scan').html(html).hide().fadeIn();
        }
        $(this).val('');
    });

    function insertProductToCartElement (id, name, code, price, inventory) {
        if (inventory == 0) return false

        toggleElProductSelected(id, true);
        arr_item_id.push(id); 
        $('#table-receipt tbody').append(`
            <tr>
                <td scope="row">1</td>
                <td class="align-middle">`+name+`</td>
                <td>`+code+`</td>
                <td>Rp. `+price+`</td>
                <td class="text-center" style="width: 136px">
                    <input type="hidden" name="items[`+index_item+`][id]" value="`+id+`" form="form-sale" />
                    <input class="input-qty-item" type="number" value="1" min="1" name="items[`+index_item+`][qty]" max="`+inventory+`" form="form-sale"/>
                </td>
                <td class="text-right">Rp. `+price+`</td>
                <td class="text-center">
                    <button class="btn btn-sm btn-danger item-delete" data-id="`+id+`"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        `);
        $('#table-receipt tbody tr:last').children().eq(4).children().last().inputSpinner();
        index_item++;
        refreshItem()

        return true;
    }

    function addQty(product_id) {
        let el = $('#table-receipt tbody tr td input[value="'+product_id+'"]').next();
        let max = parseInt(el.attr('max'))
        let current_qty = parseInt(el.val())
        if (current_qty < max) {
            let new_qty = current_qty + 1;
            el.val(new_qty)
            let price = el.parent().prev().html().split(' ').pop();
            let result = new_qty*price;
            el.parent().next().html('Rp. '+result);
            newTotalPrice(); 
        }
    }
</script>
@endpush
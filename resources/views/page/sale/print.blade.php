<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Struk</title>
    <style>
        body{
            width: 360px;
            font-family: monospace;
        }
        h3 {
            text-align: center;
        }
        p {
            margin: 4px;
        }
        table {
            width: 100%
        }
        #table-items tr:nth-child(even)>td { 
            padding-bottom: 5px;
        }
        .segment-dash{
            margin : 10px 0px;
            border-bottom: 2px dashed #767676;
        }
    </style>
</head>
<body>
    <header style="text-align: center">
        <img src="{{ asset('assets/img/ticket/logo_white.png') }}" alt="" srcset="  " style="height: 100px">
        <h3>{{ config('app.name') }}</h3>
        <p>{{ now() }}</p>
        <p>{{ auth()->user()->store ? auth()->user()->store->address : 'Kab. Tulungagung, Jawa Timur' }}</p>
        {{-- <p>Kabupaten Pacitan, Jawa Timur</p> --}}
    </header>
    <section style="margin-top: 20px">
        <table>
            <tbody>
                {{-- <tr>
                    <td style="text-align: left">25 June 2024</td>
                    <td style="text-align: right">15:10</td>
                </tr> --}}
                <tr>
                    <td style="text-align: left">Petugas Print</td>
                    <td style="text-align: right">{{ auth()->user()->fullname }}</td>
                </tr>
                <tr>
                    <td style="text-align: left">ID Transaksi</td>
                    <td style="text-align: right">{{ $transaction->id }}</td>
                </tr>
            </tbody>
        </table>
        <p class="segment-dash"></p>
        <table id="table-items">
            <tbody>
                @foreach ($transaction->items as $item)
                    <tr>
                        <td colspan="4">{{ $item->name }}</td>
                    </tr>
                    <tr style="margin-bottom : 5px">
                        <td style="text-align: right" >{{ $item->qty }}</td>
                        <td>x</td>
                        <td style="text-align: right">Rp {{ number_format($item->price,0,'','.') }}</td>
                        <td style="text-align: right">Rp {{ number_format($item->qty * $item->price,0,'','.') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <p class="segment-dash"></p>
        <table>
            <tbody style="text-align: right">
                <tr style="font-weight: bold;">
                    <td>Sub Total</td>
                    <td>Rp {{ number_format($transaction->total_price,0,'','.') }}</td>
                </tr>
                <tr>
                    <td>Diskon</td>
                    <td>- Rp {{ number_format($transaction->discount,0,'','.') }}</td>
                </tr>
                <tr style="font-weight: bold;">
                    <td>Total</td>
                    <td>Rp {{ number_format($transaction->final_amount,0,'','.') }}</td>
                </tr>
                <tr>
                    <td>Pembayaran</td>
                    <td>Rp {{ number_format($transaction->paid_amount,0,'','.') }}</td>
                </tr>
                <tr>
                    <td>Kembalian</td>
                    <td>Rp {{ number_format($transaction->change,0,'','.') }}</td>
                </tr>
            </tbody>
        </table>
    </section>
    <footer style="text-align: center;margin-top:20px">
        <p>Selamat Berbelanja.</p>
        <p>Terima Kasih.</p>
    </footer>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            window.print();
            setTimeout(function(){
                window.close();
            }, 500);
        });
    </script>
</body>
</html>
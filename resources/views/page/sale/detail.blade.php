@extends('layout.main')

@section('title', 'Detail Penjualan')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body pb-1">
                    <div class="row">
                        <div class="col-12 col-lg-9 col-xl-8">
                            <div class="table-responsive">
                                <table class="table" id="table-sale">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Item</th>
                                            <th scope="col">Kode</th>
                                            <th scope="col">Jenis</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col" class="text-center">Banyak Unit</th>
                                            <th scope="col" class="text-right">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sale->items as $item)
                                            <tr>
                                                <td scope="row">{{ $loop->iteration }}</td>
                                                <td class="align-middle">{{ $item->name }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->product->type }}</td>
                                                <td>Rp. {{ $item->price }}</td>
                                                <td class="text-center" style="width: 166px">
                                                    <input type="hidden" name="item[{{ $loop->index }}][id]" value="{{ $item->type_id }}" form="form-sale" />
                                                    <input class="input-qty-item" type="number" value="{{ $item->qty }}" min="1" name="item[{{ $loop->index }}][qty]" max="1000" form="form-sale"/>
                                                </td>
                                                <td class="text-right">Rp. {{ $item->qty * $item->price }}</td>
                                                <td>
                                                    <a href="#" onclick="return false;" class="item-delete" style="color: red" data-id="{{ $item->type_id }}"><i class="fa fa-close"></i> Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td scope="row"></td>
                                            <td colspan="6"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-add-item">Tambah Item</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3 col-xl-4">
                            <form action="{{ route('sale.update', $sale->id) }}" id="form-sale" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="PUT" />
                                <div class="form-group">
                                    <label>Nama Admin</label>
                                    <input type="text" class="form-control" disabled value="{{ $sale->user->fullname }}">
                                </div>
                                <div class="form-group">
                                    <label>Nama Pembeli</label>
                                    <input type="text" name="cust_name" class="form-control" form="form-sale" value="{{ $sale->cust_name }}">
                                </div>
                                <div class="form-group">
                                    <label>Total Harga Item</label>
                                    <div class="input-group">
                                        <span class="input-group-text" id="total_item">Rp.</span>
                                        <input type="number" name="total_item" class="form-control" form="form-sale" value="{{ $sale->total_item }}" aria-describedby="total_item" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Diskon</label>
                                    <div class="input-group">
                                        <span class="input-group-text" id="discount">Rp.</span>
                                        <input type="number" name="discount" class="form-control" form="form-sale" value="{{ $sale->discount }}" aria-describedby="discount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Total Harga Akhir</label>
                                    <div class="input-group">
                                        <span class="input-group-text" id="total_price">Rp.</span>
                                        <input type="number" name="total_price" class="form-control" form="form-sale" value="{{ $sale->total_price }}" aria-describedby="total_price" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Note.</label>
                                    <textarea name="note" rows="4" style="height: auto !important" class="form-control">{{ $sale->note }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i> Update Data Penjualan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add item-->
<div class="modal fade" id="modal-add-item" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right: 0;margin-right: 15px;margin-top: 13px;z-index: 1;width: 32px;">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <table class="table table-striped " id="table-item" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th>Nama Item</th>
                            <th>Kode</th>
                            <th>Jenis</th>
                            <th>Inventory</th>
                            <th class="text-right">Harga</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/input-spinner/bootstrap-input-spinner.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    $(".input-qty-item").inputSpinner();
</script>
<script>
    $(document).ready(function () {
        let items = $('#table-item').DataTable( {
            "processing": true,
            "scrollY"   :"500px",
            "scrollCollapse": true,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "product.name"},
                { "data": "product.code"},
                { "data": "type"},
                { "data": "inventory", "sClass": "text-center" },
                { "data": "price", "sClass": "text-right"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        // return `<button class="btn btn-sm btn-primary" onclick="addItem(`+row.id+`,`+row.name+`,`+row.code+`)"><i class="fa fa-plus"></i> add</button>`;
                        return `<button class="btn btn-sm btn-warning btn-add-item" data-id="`+row.id+`"><i class="fa fa-plus"></i> add</button>`;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-item_filter").parent().prev().html(`<h5 class="modal-title">Tambah Item Pembelian</h5>`);
                $("#table-item_filter").parent().prev().addClass('col-12 col-lg-6').removeClass('col-sm-12 col-md-6')
                $("#table-item_filter").parent().addClass('col-12 col-lg-6').removeClass('col-sm-12 col-md-6')
                $("#table-item_filter").css('text-align', 'center');
                $(".table-striped").removeClass('no-footer');
            },
            "createdRow": function (row, data, index) {
                $('td', row).eq(1).css('width', '40%');
            },
        });
        items.on( 'order.dt search.dt', function () {
            items.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#modal-add-item').on('shown.bs.modal', function (e) {
            items.columns.adjust();
        });
    });

    var arr_item = {!! $sale->items !!};
    var arr_item_id = [];
    $.each(arr_item, function (indexInArray, valueOfElement) { 
        arr_item_id.push(valueOfElement.type_id);
    });
    var index_item = {{ count($sale->items) ?? 0 }};

    $('#modal-add-item').on('click','.btn-add-item', function (e) { 
        e.preventDefault();
        let id = $(this).data("id");
        if (arr_item_id.indexOf(id) == '-1') {
            let el = $(this).parent().parent().children();
            let name = el.eq(1).html();
            let code = el.eq(2).html();
            let type = el.eq(3).html();
            let inventory = el.eq(4).html();
            let price = el.eq(5).html();
            arr_item_id.push(id); 
            $('#table-sale tbody tr:last').before(`
                <tr>
                    <td scope="row">1</td>
                    <td class="align-middle">`+name+`</td>
                    <td>`+code+`</td>
                    <td>`+type+`</td>
                    <td>Rp. `+price+`</td>
                    <td class="text-center" style="width: 166px">
                        <input type="hidden" name="item[`+index_item+`][id]" value="`+id+`" form="form-sale" />
                        <input class="input-qty-item" type="number" value="1" min="1" name="item[`+index_item+`][qty]" max="`+inventory+`" form="form-sale"/>
                    </td>
                    <td class="text-right">Rp. `+price+`</td>
                    <td>
                        <a href="#" onclick="return false;" class="item-delete" style="color: red" data-id="`+id+`"><i class="fa fa-close"></i> Hapus</a>
                    </td>
                </tr>
            `);
            $('#table-sale tbody tr:last').prev('tr').children().eq(5).children().last().inputSpinner();
            index_item++;
            $('#modal-add-item').modal('hide');
            refreshItem()
        } else {
            alert('item sudah ditambahkan sebelumnya.')
        }
    });

    $('#table-sale tbody').on('change','.input-qty-item', function (e) { 
        e.preventDefault();
        let qty = $(this).val();
        let price = $(this).parent().prev().html().split(' ').pop();
        let result = qty*price;
        $(this).parent().next().html('Rp. '+result);
        newTotalItem();
    });

    $('#table-sale tbody').on('click','.item-delete', function (e) { 
        e.preventDefault()
        let id = $(this).data("id");
        
        arr_item_id = jQuery.grep(arr_item_id, function(value) {
            return value != id;
        });
        $(this).parent().parent().remove();
        refreshItem();
    });

    function refreshItem() {
        numberingItem();
        newTotalItem();
    }

    function numberingItem() {
        let count = 1;
        $('#table-sale tbody tr:not(:last-child)').each(function() {
            $(this).find("td:first").html(count);
            count++;
        });
    };

    function newTotalItem() {
        let amount = 0;
        $('#table-sale tbody tr:not(:last-child)').each(function() {
            amount += ~~$(this).find('td').eq(6).html().split(' ').pop();
        });
        $('#form-sale [name="total_item"]').val(amount);
        let discount = ~~$('#form-sale [name="discount"]').val();
        $('#form-sale [name="discount"]').parent().parent().next().find('input').val(amount - discount);
    }

    $('#form-sale').on('change keyup', '[name="discount"]', function () {
        newTotalItem();
    })

</script>
@endpush
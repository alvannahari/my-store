@extends('layout.main')

@section('title', 'Detail Supplier')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('supplier.update', $supplier->id) }}" id="form-supplier" method="POST">
                    <input type="hidden" name="_method" value="PUT" />
                    <div class="card-body pb-1">
                        @csrf
                        <input type="hidden" name="_method" value="PUT" />
                        <div class="form-group">
                            <label>Nama Supplier</label>
                            <input type="text" class="form-control" name="fullname" required value="{{ $supplier->fullname }}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="address" required value="{{ $supplier->address }}">
                        </div>
                        <div class="form-group">
                            <label>No. Identitas</label>
                            <input type="text" class="form-control" name="identity_id" value="{{ $supplier->identity_id }}">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi Kategori</label>
                            <textarea name="desc" rows="4" class="form-control" style="height: auto !important">{{ $supplier->desc }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>No. Handphone</label> 
                            <input type="number" class="form-control" value="{{ $supplier->mobile_number }}" name="mobile_number">
                        </div>
                        <div class="form-group">
                            <label>Created At</label> 
                            <input type="text" class="form-control" value="{{ $supplier->created_at }}" disabled>
                        </div>
                        <div class="form-group">
                            <label>Updated At</label> 
                            <input type="text" class="form-control" value="{{ $supplier->updated_at }}" disabled>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Simpan Perubahan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
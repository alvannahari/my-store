@extends('layout.main')

@section('title', 'Detail Kategori Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('category.update', $category->id) }}" id="form-category" method="POST">
                    <input type="hidden" name="_method" value="PUT" />
                    <div class="card-body pb-1">
                        @csrf
                        <div class="form-group">
                            <label>Nama Toko</label>
                            @auth('admin')
                                <select name="store_id" class="form-control">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}" {{ $category->store_id == $store->id ? 'selected' : '' }}>{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" name="store_id" value="{{ $stores->id }}">
                                <input type="text" class="form-control" value="{{ $stores->name }}" disabled>
                            @endauth
                        </div>
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" class="form-control" name="name" required value="{{ $category->name }}">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi Kategori</label>
                            <textarea name="desc" rows="4" class="form-control" style="height: auto !important">{{ $category->desc }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Produk</label> 
                            <input type="number" class="form-control" value="{{ $category->products_count }}" disabled>
                        </div>
                        <div class="form-group">
                            <label>Created At</label> 
                            <input type="text" class="form-control" value="{{ $category->created_at }}" disabled>
                        </div>
                        <div class="form-group">
                            <label>Updated At</label> 
                            <input type="text" class="form-control" value="{{ $category->updated_at }}" disabled>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Simpan Perubahan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
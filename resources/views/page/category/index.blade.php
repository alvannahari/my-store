@extends('layout.main')

@section('title', 'Kategori Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                {{-- <div class="card-header">
                    <h4>Basic DataTables</h4>
                </div> --}}
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-category" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama Kategori</th>
                                    <th>Deskripsi</th>
                                    <th>Nama Toko</th>
                                    <th class="text-center">Jumlah Produk</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Category-->
<div class="modal fade" id="modal-add-category" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Kategori Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form class="mt-3" method="POST" id="form-add-category">
                    @csrf
                    <div class="form-group">
                        <label>Nama Toko</label>
                        @auth('admin')
                            <select name="store_id" class="form-control">
                                @foreach ($stores as $store)
                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="hidden" name="store_id" value="{{ $stores->id }}">
                            <input type="text" class="form-control" value="{{ $stores->name }}" disabled>
                        @endauth
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" placeholder="nama kategori" name="name">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="desc" rows="5" class="form-control" placeholder="deskripsi singkat tentang kategori ini"></textarea>
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btn-add-category">Tambah</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Category -->
<div class="modal fade" id="modal-edit-category" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form-edit-category">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Toko</label>
                        @auth('admin')
                            <select name="store_id" class="form-control">
                                @foreach ($stores as $store)
                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="hidden" name="store_id" value="{{ $stores->id }}">
                            <input type="text" class="form-control" value="{{ $stores->name }}" disabled>
                        @endauth
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" placeholder="nama kategori" name="name">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="desc" rows="5" class="form-control" placeholder="deskripsi singkat tentang kategori ini"></textarea>
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-update-category">Update Kategori</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete Category -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus kategori ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="btn-delete-category">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    var category_id = 0;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"}
        })
        
        let dt_categories = $('#table-category').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name"},
                { "data": "desc"},
                { "data": "store.name"},
                { "data": "products_count", "sClass": "text-center"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (!"{!! Auth::guard('cashier')->check() !!}") {
                            return `
                                <button data-id="`+row.id+`" class="btn btn-sm btn-warning btn-edit-category" title="update"><i class="fas fa-pencil-alt"></i> Update</button>
                                <button class="btn btn-sm btn-danger" onclick="deleteCategory(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                                `;
                        } else {return null}
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                if (!"{!! Auth::guard('cashier')->check() !!}") {
                    $("#table-category_length").parent().before(`
                        <div class="col-12 col-md-4 text-center text-md-left">
                            <button class="btn btn-rounded btn-success mb-2" type="button" data-toggle="modal" data-target="#modal-add-category"><i class="fa fa-plus"></i> Tambah Kategori Produk</button>
                        </div>
                    `);
                    $("#table-category_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                    $("#table-category_length").css('text-align', 'center');
                    $("#table-category_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                }
            }   
        });
        dt_categories.on( 'order.dt search.dt', function () {
            dt_categories.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-add-category').click(function (e) { 
            e.preventDefault();
            let btn = $(this)

            $.ajax({
                type: "POST",
                url: "{{ route('category.store') }}",
                data : $('#form-add-category').serialize(),
                beforeSend: function() {
                    btn.addClass('btn-progress')
                },
                success: function (response) {
                    if (response.status) {
                        dt_categories.ajax.reload(function () {
                            $('#modal-add-category').modal('hide');
                            showAlert('success',response.message,'Kategori Baru')
                            clearForm();
                        }, false);
                    }
                    else {
                        showAlert('error','Silakan periksa formulir','Kategori Baru')
                        for (let key of Object.keys(response.errors)) {
                            $('#form-add-category [name="'+key+'"]').addClass('is-invalid');
                            $('#form-add-category [name="'+key+'"]').next().html(response.errors[key]);
                        }
                    }
                    btn.removeClass('btn-progress')
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    showAlert('error',XMLHttpRequest.responseJSON.message,'Kategori Baru')
                    console.log(XMLHttpRequest.responseJSON.message);
                    btn.removeClass('btn-progress')
                }, 
            });
        });

        $('#table-category').on('click','.btn-edit-category', function () { 
            category_id = $(this).data("id");
            let name = $(this).parent().parent().children('td:nth-child(2)').html()
            let desc = $(this).parent().parent().children('td:nth-child(3)').html()
            let store = $(this).parent().parent().children('td:nth-child(4)').html()
            $('#modal-edit-category select[name=store_id] option').each(function() { this.selected = (this.text == store); });
            $('#modal-edit-category input[name=name]').val(name)
            $('#modal-edit-category textarea[name=desc]').val(desc)
            $('#modal-edit-category').modal('show');
        });

        $('#btn-update-category').click(function (e) { 
            e.preventDefault();
            let btn = $(this)
            let url_update = "{{ route('category.update', ':id') }}";

            $.ajax({
                type: "POST",
                url: url_update.replace(':id', category_id),
                data : $('#form-edit-category').serialize(),
                beforeSend: function() {
                    btn.addClass('btn-progress')
                },
                success: function (response) {
                    if (response.status) {
                        dt_categories.ajax.reload(function () {
                            $('#modal-edit-category').modal('hide');
                            showAlert('success',response.message,'Update Kategori')
                            clearForm();
                        }, false);
                    }
                    else {
                        showAlert('error','Terjadi Kesalahan','Update Kategori')
                        for (let key of Object.keys(response.errors)) {
                            $('#form-edit-category [name="'+key+'"]').addClass('is-invalid');
                            $('#form-edit-category [name="'+key+'"]').next().html(response.errors[key]);
                        }
                    }
                    btn.removeClass('btn-progress')
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    showAlert('error',XMLHttpRequest.responseJSON.message,'Update Kategori')
                    console.log(XMLHttpRequest.responseJSON.message);
                    btn.removeClass('btn-progress')
                }, 
            });
        });

        $('#btn-delete-category').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('category.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', category_id),
                success: function (response) {
                    if (response.status) {
                        dt_categories.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Hapus Kategori')
                        }, false);
                    }
                },
            });
        });
    });

    function deleteCategory(id) {
        category_id = id;
        $('#modal-delete').modal('show');
    }
</script>
@endpush


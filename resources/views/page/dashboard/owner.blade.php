@extends('layout.main')

@section('title', 'Dashboard')

@section('content')
<section class="section">
    <div class="row ">
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Jumlah Produk</h5>
                                    <h2 class="mb-3 font-18">{{ $daily['items'] }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-green">10%</span> 
                                        Meningkat
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="{{ asset('assets/img/banner/1.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15"> Penjualan Hari Ini</h5>
                                    <h2 class="mb-3 font-18">{{ $daily['sales'] }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-orange">09%</span> 
                                        Menurun
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="{{ asset('assets/img/banner/2.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Pendapatan Hari Ini</h5>
                                    <h2 class="mb-3 font-18">Rp. {{ number_format($daily['revenue'],0,',','.') }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-green">18%</span>
                                        Meningkat
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="{{ asset('assets/img/banner/3.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Penghasilan Hari Ini</h5>
                                    <h2 class="mb-3 font-18">Rp. {{ number_format($daily['income'],0,',','.') }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-green">42%</span> 
                                        Meningkat
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="{{ asset('assets/img/banner/4.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>10 Produk Terlaris</h4>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped mb-0">
                        <tr>
                            <th class="text-center">No.</th>
                            <th></th>
                            <th>Nama</th>
                            <th>Jenis</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th>Total</th>
                            <th class="text-center">Updated At</th>
                            <th></th>
                        </tr>
                        @forelse ($sellers as $seller)
                            <tr>
                                <td class="p-0 text-center">{{ $loop->iteration }}</td>
                                <td>
                                    <img alt="image" src="{{ $seller['product']['image'] }}" width="55">
                                </td>
                                <td>{{ $seller['product']['name'] }}</td>
                                <td>{{ $seller['type'] }}</td>
                                <td>Rp. {{ $seller['price'] }}</td>
                                <td class="align-middle">
                                    <div class="progress-text">{{ $seller['percent'] }}</div>
                                    <div class="progress" data-height="6">
                                        @if ($seller['percent'] >= 70)
                                            <div class="progress-bar bg-purple" data-width="{{ $seller['percent'] }}%"></div>
                                        @elseif ($seller['percent'] >= 50)
                                            <div class="progress-bar bg-success" data-width="{{ $seller['percent'] }}%"></div>
                                        @elseif ($seller['percent'] >= 25)
                                            <div class="progress-bar bg-orange" data-width="{{ $seller['percent'] }}%"></div>
                                        @else
                                            <div class="progress-bar bg-danger" data-width="{{ $seller['percent'] }}%"></div>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $seller['sale_items_count'] }}</td>
                                <td class="text-center">{{ $seller['product']['updated_at'] }}</td>
                                <td><a href="{{ route('product.show', $seller['product_id']) }}" class="btn btn-sm btn-outline-warning">Detail</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center text-muted" colspan="8"><h6>Belum ada produk terjual.</h6></td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h4>Riwayat Produk Terjual Terakhir</h4>
                </div>
                <div class="card-body p-0">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th>Nama</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th class="text-center">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($sold as $history)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $history->name }} </td>
                                    <td>{{ $history->qty }}</td>
                                    <td>Rp. {{ $history->price }}</td>
                                    <td class="text-center">{{ date('d-M-y', strtotime($history->created_at)) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center text-muted" colspan="5"><h6>Belum ada produk terjual.</h6></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('addons-script')
@endpush
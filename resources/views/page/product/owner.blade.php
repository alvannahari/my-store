@extends('layout.main')

@section('title', 'Daftar Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product.import') }}" method="POST" id="form-import" style="display: none" enctype='multipart/form-data'>
                        @csrf
                        <input type="file" id="input-file-import" name="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                    </form>
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-product" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th></th>
                                    <th>ID Barang</th>
                                    <th>Nama</th>
                                    <th>Kode Barang</th>
                                    <th class="text-right">Inventori</th>
                                    <th class="text-right">Harga Asli</th>
                                    <th class="text-right">Harga Jual</th>
                                    <th class="text-center">Updated At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Show Image-->
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content" style="background: #ffffff00;border: none;box-shadow: none;">
            <div class="text-center">
                <img src="" alt="" srcset="" style="width: auto; max-width:100%; max-height : 900px;">
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete Product-->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('product.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE" />
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus product ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<style>
    table.dataTable tbody th, table.dataTable tbody td:nth-child(2) {
        padding: 3px;
    }
    table.dataTable tbody th, table.dataTable tbody td:nth-child(2):hover {
        cursor: pointer;
        filter: brightness(95%);
    }
</style>
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    $(document).ready(function () {

        let t = $('#table-product').DataTable( {
            processing: true,
            pageLength: 25,
            ajax: "{!! url()->current() !!}",
            order : [[3, 'asc']],
            columns: [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `<img alt="image" src="`+row.image+`" width="75" style="max-height:75px;">`;
                    },"bSortable": false,
                },
                { "data": "product_id"},
                { "data": "name"},
                { "data": "code"},
                { "data": "inventory", "sClass": "text-right"},
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                    return formatRupiah(row.capital_price);
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return formatRupiah(row.price);
                    }
                },
                { "data": "updated_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('product.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.product_id)+`" class="btn btn-sm btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-sm btn-danger" onclick="deleteProduct('`+row.product_id+`')"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-product_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <a class="btn btn-rounded btn-success mb-2" type="button" href="{{ route('product.create') }}"><i class="fa fa-plus"></i> Tambah Produk Baru</a>
                        <button class="btn btn-info mb-2" onclick="importProduct()"><i class="fa fa-print me-1"></i> Import</button>
                    </div>
                `);
                $("#table-product_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-product_length").css('text-align', 'center');
                $("#table-product_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }   
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#table-product').on('click','img', function() {
            let me = $(this);
            $('#modal-image img').attr('src', me.attr('src'));
            $('#modal-image').modal('show');
        });
        $('#modal-image .modal-content').click(function (e) { 
            e.preventDefault();
            $('#modal-image').modal('hide');
        });
    });

    function deleteProduct(id) {
        let url_delete = "{{ route('product.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }

    function importProduct() {
        $('#input-file-import').click();
    }

    $('#input-file-import').on('change', function(){ 
        if ($(this).get(0).files.length != 0) {
            $( "#form-import" ).submit();
        }
    });

    
</script>
@endpush


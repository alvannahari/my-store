@extends('layout.main')

@section('title', 'Tambah Produk Baru')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('product.store') }}" method="POST" id="form-product" enctype="multipart/form-data">
                    <div class="card-body pb-1">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input type="text" name="name" class="form-control" placeholder=". . . ." required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Kode Produk</label>
                                    <input type="text" name="code" class="form-control" placeholder=". . . .">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Nama Store</label>
                                    @auth('admin')
                                        <select name="store_id" class="form-control">
                                            @foreach ($stores as $store)
                                                <option value="{{ $store->id }}">{{ $store->name }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <input type="hidden" name="store_id" value="{{ $stores->id }}" class="form-control">
                                        <input type="text" disabled value="{{ $stores->name }}" class="form-control">
                                    @endauth
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Nama Kategori</label>
                                    <select name="category_id" class="form-control">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="desc" rows="4" style="height: auto !important" class="form-control"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Inventory</label>
                                    <input type="number" name="inventory" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Harga Modal</label>
                                    <input type="number" name="capital_price" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Harga Jual Eceran</label>
                                    <input type="number" name="price" class="form-control" required value="0">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Gambar</label>
                                    <input type="file" name="image" class="form-control" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i> Tambah Produk</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@extends('layout.main')

@section('title', 'Daftar Produk Toko Lain')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-product" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th></th>
                                    <th>Toko</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th class="text-right">Harga Jual</th>
                                    <th class="text-center">Updated At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Show Image-->
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content" style="background: #ffffff00;border: none;box-shadow: none;">
            <div class="text-center">
                <img src="" alt="" srcset="" style="width: auto; max-width:100%; max-height : 900px;">
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<style>
    table.dataTable tbody th, table.dataTable tbody td:nth-child(2) {
        padding: 3px;
    }
    table.dataTable tbody th, table.dataTable tbody td:nth-child(2):hover {
        cursor: pointer;
        filter: brightness(98%);
    }
</style>
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script>
    $(document).ready(function () {
        let t = $('#table-product').DataTable( {
            "processing": true,
            "pageLength": 25,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `<img alt="image" src="`+row.image+`" width="75">`;
                    },"bSortable": false
                },
                { "data": "store"},
                { "data": "name"},
                { "data": "code"},
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return formatRupiah(row.price);
                    }
                },
                { "data": "updated_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('product.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.product_id)+`" class="btn btn-sm btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            `;
                    },"bSortable": false
                }
            ],
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#table-product').on('click','img', function() {
            let me = $(this);
            $('#modal-image img').attr('src', me.attr('src'));
            $('#modal-image').modal('show');
        });
        $('#modal-image .modal-content').click(function (e) { 
            e.preventDefault();
            $('#modal-image').modal('hide');
        });
    });
</script>
@endpush


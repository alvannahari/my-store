@extends('layout.main')

@section('title', 'Detail Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Informasi Produk</h4>
                </div>
                <form action="{{ route('product.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    <div class="card-body pt-2 pb-1">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input type="text" name="name" class="form-control" placeholder=". . . ." required value="{{ $product->name }}">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Kode Produk</label>
                                    <input type="text" name="code" class="form-control" placeholder=". . . ." value="{{ $product->code }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Nama Store</label>
                                    <input type="text" disabled value="{{ $product->category->store->name }}" class="form-control">
                                    {{-- <select name="store_id" class="form-control">
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}" {{ $product->category->store_id == $store->id ? 'selected' : '' }}>{{ $store->name }}</option>
                                        @endforeach
                                    </select> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label>Nama Kategori</label>
                                    <select name="category_id" class="form-control">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" {{ $product->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="desc" rows="4" style="height: auto !important" class="form-control">{{ $product->desc }}</textarea>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Created At</label>
                                    <input type="text" class="form-control" disabled value="{{ $product->created_at }}">
                                </div>
                                <div class="form-group">
                                    <label>Updated At</label>
                                    <input type="text" class="form-control" disabled value="{{ $product->updated_at }}">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Gambar Produk</label>
                                    <div style="cursor: pointer;">
                                        <img src="{{ $product->image }}" alt="" width="138" class="img-responsive thumbnail" style="display: block;height: 138px;width: auto;" id="image-product">
                                    </div>
                                    <input type="file" name="image" class="form-control" style="display: none" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (auth()->guard('admin')->check() || $owner == true)
                        <div class="card-footer pt-0">
                            <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i> Simpan Data Produk</button>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Harga Produk Berlaku</h4>
                </div>
                <div class="card-body pt-2">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Jenis</th>
                                <th>QTY</th>
                                <th>Minimal</th>
                                @if (auth()->guard('admin')->check() || $owner == true)
                                    <th>Modal</th>
                                @endif
                                <th>Harga</th>
                                <th class="text-center">Tanggal</th>
                                @if (auth()->guard('admin')->check() || $owner == true)
                                    <th></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($current as $key => $type)
                                @php $count = $loop->index @endphp
                                @foreach ($type as $price)
                                    <tr>
                                        <td>{{ 2 * $count + $loop->iteration }}</td>
                                        <td>{{ $key }}</td>
                                        <td>{{ $price['inventory'] }}</td>
                                        <td>{{ $price['min'] == '1' ? 'Satuan' : $price['min'] }}</td>
                                        @if (auth()->guard('admin')->check() || $owner == true)
                                            <td>{{ number_format($price['capital_price'],0,',','.') }}</td>
                                        @endif
                                        <td>{{ number_format($price['price'],0,',','.') }}</td>
                                        <td class="text-center">{{ date('j M y - H:i', strtotime($price['created_at'] )) }}</td>
                                        @if (auth()->guard('admin')->check() || $owner == true)
                                            <td><button class="btn btn-sm btn-danger" onclick="deletePrice({{ $price['id'] }})"><i class="fa fa-trash"></i></button></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="5"><h6>Harga belum ditemukan</h6></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @if (auth()->guard('admin')->check() || $owner == true)
                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal-add-price">Tambah data harga</button>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Riwayat Harga Produk</h4>
                </div>
                <div class="card-body pt-2">
                    <div class="table-wrapper" style="width: 100%;max-height: 400px;overflow:auto;display:inline-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Jenis</th>
                                    <th>Minimal</th>
                                    @if (auth()->guard('admin')->check() || $owner == true)
                                        <th>Modal</th>
                                    @endif
                                    <th>Harga</th>
                                    <th class="text-center">Tanggal</th>
                                    @if (auth()->guard('admin')->check() || $owner == true)
                                        <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($histories as $key => $type)
                                    @foreach ($type as $price)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $key }}</td>
                                            <td>{{ $price['min'] == '1' ? 'Satuan' : $price['min'] }}</td>
                                            @if (auth()->guard('admin')->check() || $owner == true)
                                                <td>{{ number_format($price['capital_price'],0,',','.') }}</td>
                                            @endif
                                            <td>{{ number_format($price['price'],0,',','.') }}</td>
                                            <td class="text-center">{{ date('j M y - H:i', strtotime($price['created_at'] )) }}</td>
                                            @if (auth()->guard('admin')->check() || $owner == true)
                                                <td><button class="btn btn-sm btn-danger" onclick="deletePrice({{ $price['id'] }})"><i class="fa fa-trash"></i></button></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @empty
                                    <tr>
                                        <td class="text-muted text-center" colspan="4"><h6>Riwayat belum ditemukan</h6></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Price-->
<div class="modal fade" id="modal-add-price" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah / Perbarui Harga</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" method="POST" action="{{ route('product-price.store') }}" >
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Jenis</label>
                                <input type="text" class="form-control" placeholder="Ex. Ecer" name="type" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Inventory</label>
                                <input type="number" class="form-control" placeholder="999" name="inventory">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label>Modal</label>
                                <input type="number" class="form-control" min="0" placeholder="10000" name="capital_price" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Minimal</label>
                                <input type="number" class="form-control" min="1" name="min" value="1" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="number" class="form-control" min="0" placeholder="10000" name="price" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Tambah / Perbarui</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Product-->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('product-price.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE" />
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus harga ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<style>
    .table td {
        height: 43px !important;
    }
</style>
@endpush

@push('addons-script')
<script>
    $('#image-product').click(function (e) { 
        e.preventDefault();
        $('[name="image"]').trigger('click'); 
    });

    $('[name="image"]').on('change', function(e) {
        e.preventDefault();
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-product').attr('src', e.target.result).hide().fadeIn(700);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function deletePrice(id) {
        let url_delete = "{{ route('product-price.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }
</script>
@endpush
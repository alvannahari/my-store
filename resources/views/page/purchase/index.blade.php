@extends('layout.main')

@section('title', 'Riwayat Pembelian')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-purchase" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Judul</th>
                                    <th>Nama Toko</th>
                                    <th>Supplier</th>
                                    <th>Jenis Barang</th>
                                    <th>Total Harga</th>
                                    <th class="text-center">Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add Purchases-->
<div class="modal fade" id="modal-add-purchase" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Pembelian Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" method="POST" action="{{ route('purchase.store') }}" id="form-purchase" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Judul</label>
                                <input type="text" class="form-control" placeholder="Belanja di Surya" name="name" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control" name="date" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Nama Toko</label>
                                @auth('admin')
                                    <select name="store_id" class="form-control">
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <input type="hidden" name="store_id" value="{{ $stores->id }}" class="form-control">
                                    <input type="text" disabled value="{{ $stores->name }}" class="form-control">
                                @endauth
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Supplier</label>
                                <select name="supplier_id" class="form-control">
                                    @if (count($suppliers) > 0)
                                        <option value="0">Suplier tidak dikenal</option>
                                    @endif
                                    @forelse ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}">{{ $supplier->fullname }}</option>
                                    @empty 
                                        <option value="0">Suplier belum tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Banyak Barang</label>
                                <input type="number" class="form-control" name="amount" required placeholder="10">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Total Harga</label>
                                <div class="input-group">
                                    <span class="input-group-text" id="total_price">Rp.</span>
                                    <input type="number" name="total_price" class="form-control" min="1" aria-describedby="total_price" placeholder="100000">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <label>Gambar</label>
                        <div class="row input-image">
                            <div class="col-6 text-center mb-2">
                                <input type="file" name="image[0]" style="display: none" accept="image/png, image/gif, image/jpeg">
                                <div style="vertical-align: middle;cursor:pointer;background-color: #efefef;width: 100%;height: 200px" id="btn-add-image">
                                    <div class="container" style="opacity: .4;padding-top: 30px;">
                                        <img src="{{ asset('assets/img/add_image.png') }}" alt="" srcset="" style="height:100px">
                                    </div>
                                    <h6 class="mt-3" style="color: #9e9e9e">Tambah Gambar</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('purchase.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE" />
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus riwayat penjualan ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script>
    $(document).ready(function () {
        moment.locale('id');
        let t = $('#table-purchase').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name"},
                { "data": "store.name"},
                { "data": "supplier_name"},
                { "data": "amount", "sClass": "text-center" },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return formatRupiah(row.total_price);
                    }
                },
                { "data": "date", "sClass": "text-center", render: function(data, type, row){
                        if(type === "sort" || type === "type"){
                            return data;
                        }
                        return moment(data).format("dddd D MMMM YYYY");
                    }
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('purchase.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-warning" title="detail"><i class="fas fa-pencil-alt"></i> Detail</a>
                            <button class="btn btn-danger" onclick="deletePurchase(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-purchase_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-success mb-2" data-toggle="modal" data-target="#modal-add-purchase"><i class="fa fa-plus"></i> Tambah Pembelian</button>
                    </div>
                `);
                $("#table-purchase_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
                $("#table-purchase_length").css('text-align', 'center');
                $("#table-purchase_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6');
            }   
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deletePurchase(id) {
        let url_delete = "{{ route('purchase.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }

    var arr_image = 0;
    $('#form-purchase').on('click', '#btn-add-image', function(e) {
        e.preventDefault();
        $(this).prev().click();
    })

    $('#form-purchase .input-image').on('change', '[type="file"]:last', function(e) {
        e.preventDefault();
        $(this).next().remove();
        readURL(this);
        arr_image++
        $(this).parent().after(`
            <div class="col-6 text-center mb-2">
                <input type="file" name="image[`+arr_image+`]" style="display: none" accept="image/png, image/gif, image/jpeg">
                <div style="vertical-align: middle;cursor:pointer;background-color: #efefef;width: 100%;height: 200px" id="btn-add-image">
                    <div class="container" style="opacity: .4;padding-top: 30px;">
                        <img src="{{ asset('assets/img/add_image.png') }}" alt="" srcset="" style="height:100px">
                    </div>
                    <h6 class="mt-3" style="color: #9e9e9e">Tambah Gambar</h6>
                </div>
            </div>
        `)
    })

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).after(`
                    <img src="`+e.target.result+`" alt="" style="width: 100%;border-radius:6px;max-height:260px">
                    <button class="btn btn-delete btn-sm btn-rounded btn-danger my-2" type="button"><i class="fa fa-trash"></i> Hapus Gambar</button>
                `);
                // $(input).next().fadeIn();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#form-purchase .input-image').on('click', '.btn-delete', function(e) {
        $(this).parent().fadeOut('slow', function() {
            $(this).remove();
        });
    })
</script>
@endpush


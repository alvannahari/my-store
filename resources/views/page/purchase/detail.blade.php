@extends('layout.main')

@section('title', 'Detail Pembelian')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('purchase.update', $purchase->id) }}" method="POST" enctype="multipart/form-data">
                    <div class="card-body pb-1">
                        <input type="hidden" name="_method" value="PUT" />
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" name="name" class="form-control" placeholder=". . . ." required value="{{ $purchase->name }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input type="date" name="date" class="form-control" value="{{ $purchase->date }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Nama Toko</label>
                                    @auth('admin')
                                        <select name="store_id" class="form-control">
                                            @foreach ($stores as $store)
                                                <option value="{{ $store->id }}" {{ $purchase->store_id == $store->id ? 'selected' : '' }}>{{ $store->name }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <input type="hidden" name="store_id" value="{{ $stores->id }}" class="form-control">
                                        <input type="text" disabled value="{{ $stores->name }}" class="form-control">
                                    @endauth
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <select name="supplier_id" class="form-control">
                                    @if ($purchase->supplier_id == 0)
                                        <option value="0" selected>-- supplier belum tersedia --</option>
                                    @endif
                                    @foreach ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}" {{ $purchase->supplier_id == $supplier->id ? 'selected' : '' }}>{{ $supplier->fullname }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Banyak Barang</label>
                                    <input type="number" name="amount" class="form-control" value="{{ $purchase->amount }}">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Total Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-text" id="total_price">Rp.</span>
                                        <input type="number" name="total_price" class="form-control" min="0" value="{{ $purchase->total_price }}" aria-describedby="total_price">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i> Simpan Data Pembelian</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix mx-0 text-center">
                            @foreach ($purchase->images as $image)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-2" >
                                <a href="{{ $image->image }}" data-sub-html="Demo Description">
                                    <img class="my-0" style="width:100%;max-height:270px" src="{{ $image->image }}" alt="">
                                </a>
                                <button class="btn btn-delete btn-sm btn-rounded btn-danger my-2" type="button" onclick="deletePurchase({{ $image->id }})"><i class="fa fa-trash"></i> Hapus Gambar</button>
                            </div>
                            {{-- <div class="col-6 col-md-4 col-lg-3 col-xl-2 text-center">
                                <img src="{{ $image->image }}" style="width: 100%;height:auto;max-height:270px">
                                <button class="btn btn-delete btn-sm btn-rounded btn-danger my-2" type="button" onclick="deletePurchase({{ $image->id }})"><i class="fa fa-trash"></i> Hapus Gambar</button>
                            </div> --}}
                            @endforeach
                            <div class="col-6 col-md-4 col-lg-3 col-xl-2 text-center">
                                <form action="{{ route('purchase-image.store', $purchase->id) }}" method="POST" enctype="multipart/form-data" id="form-image">  
                                    @csrf
                                    <input type="hidden" name="purchase_id" value="{{ $purchase->id }}">
                                    <input type="file" name="image" style="display: none" accept="image/png, image/gif, image/jpeg">
                                </form>
                                <div style="vertical-align: middle;cursor:pointer;background-color: #efefef;width: 100%;height: 200px" id="btn-add-image">
                                    <div class="container" style="opacity: .4;padding-top: 17%;">
                                        <img src="{{ asset('assets/img/add_image.png') }}" alt="" srcset="" style="width: 40%">
                                    </div>
                                    <h6 class="mt-3" style="color: #9e9e9e">Tambah Gambar</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('purchase-image.destroy',1) }}" method="POST" id="form-delete">
                @csrf
                <input type="hidden" name="_method" value="DELETE" />
                <div class="modal-body">
                    Apakah anda yakin ingin menghapus gambar ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link href="{{ asset('assets/bundles/lightgallery/dist/css/lightgallery.css') }}" rel="stylesheet">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/lightgallery/dist/js/lightgallery-all.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/light-gallery.js') }}"></script>
<script>
    $('#btn-add-image').click(function (e) { 
        e.preventDefault();
        $('[name="image"]').trigger('click'); 
    });

    $('[name="image"]').on('change', function(e) {
        e.preventDefault();
        $('#form-image').submit();
    });

    function deletePurchase(id) {
        let url_delete = "{{ route('purchase-image.destroy', ':id') }}";
        $('#form-delete').attr('action', url_delete.replace(':id', id));
        $('#modal-delete').modal('show');
    }
</script>
@endpush
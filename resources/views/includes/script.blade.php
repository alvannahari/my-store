<!-- General JS Scripts -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<!-- JS Libraies -->
<!-- Page Specific JS File -->
<!-- Template JS File -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<!-- Custom JS File -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/bundles/izitoast/js/iziToast.min.js') }}"></script>

<script>
    setTimeout(() => {
        if( $('#notifications-state > *').length > 0 ) {
            let el = $('#notifications-state :first-child');
            let status = el.attr("data-status");
            let message = el.attr("data-message");
            
            if (status != 'error')
                showAlert('success', message, status);
            else 
                showAlert('error', message, 'error');
        }
    }, 500);

    function showAlert(status, message, title) {
        $data = {
            title: title,
            message: message,
            position: 'topRight'
        }

        if (status == 'success')
            iziToast.success($data)
        else 
            iziToast.error($data)
    }

    const formatRupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
            { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
    }

    const popupCenter = ({url, title, w, h}) => {
        // Fixes dual-screen position                             Most browsers      Firefox
        const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - w) / 2 / systemZoom + dualScreenLeft
        const top = (height - h) / 2 / systemZoom + dualScreenTop
        const newWindow = window.open(url, title, 
        `
        scrollbars=yes,
        width=${w / systemZoom}, 
        height=${h / systemZoom}, 
        top=${top}, 
        left=${left}
        `
        )

        if (window.focus) newWindow.focus();
    }

    $('.main-content').on('click', 'button', function () { 
        $('form').find('.is-invalid').removeClass('is-invalid');
        $('form').find('.is-invalid').next().html('');
    })

    function clearForm() {
        $('form input:not([name=_token],[name=_method])').val('')
        $('form textarea').val('')
        $("form select").val($("form select option:first").val());
    }
</script>
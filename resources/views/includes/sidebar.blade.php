<li class="menu-header">Data Menu</li>

<li class="dropdown {{ request()->is('/') ? 'active' : '' }}">
    <a href="{{ route('home') }}" class="nav-link"><i data-feather="monitor"></i><span>Dashboard Harian</span></a>
</li>

<li class="dropdown {{ request()->is('category*') ? 'active' : '' }}">
    <a href="{{ route('category.index') }}" class="nav-link"><i data-feather="calendar"></i><span>Kategori Produk</span></a>
</li>

@guest('cashier')
    <li class="dropdown {{ request()->is('product*') ? 'active' : '' }}">
        <a href="#" class="menu-toggle nav-link has-dropdown">
            <i data-feather="grid"></i>
            <span>Produk</span>
        </a>
        <ul class="dropdown-menu"> 
            <li class="{{ request()->is('product*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('product.index') }}">Produk Saya</a>
            </li>
            <li class="{{ request()->is('product-others') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('product.others') }}">Produk Toko Lain</a>
            </li>
        </ul>
    </li>
@endguest

@auth('cashier')
    <li class="dropdown {{ request()->is('product*') ? 'active' : '' }}">
        <a href="{{ route('product.index') }}" class="nav-link"><i data-feather="grid"></i><span>Produk</span></a>
    </li>
@endauth

<li class="dropdown {{ request()->is('transaction/create') ? 'active' : '' }}">
    <a href="{{ route('transaction.create') }}" class="nav-link"><i data-feather="shopping-bag"></i><span>Transaksi Baru</span></a>
</li>

<li class="dropdown {{ request()->is('sale/create') ? '' : (request()->is('sale*') ? 'active' : '') }}">
    <a href="{{ route('sale.index') }}" class="nav-link"><i data-feather="shopping-bag"></i><span>Riwayat Transaksi</span></a>
</li>

@guest('cashier')
<li class="dropdown {{ request()->is('purchase*') ? 'active' : '' }}">
    <a href="{{ route('purchase.index') }}" class="nav-link"><i data-feather="shopping-cart"></i><span>Pembelian Produk</span></a>
</li>

<li class="dropdown {{ request()->is('return-product*') ? 'active' : '' }}">
    <a href="{{ route('return-product.index') }}" class="nav-link"><i data-feather="rotate-ccw"></i><span>Pengembalian Produk</span></a>
</li>

<li class="menu-header">Data Master</li>

<li class="dropdown {{ request()->is('cashier*') || request()->is('owner*') || request()->is('supplier*') || request()->is('store*') ? 'active' : '' }}">
    <a href="#" class="menu-toggle nav-link has-dropdown">
        <i data-feather="users"></i>
        <span>Manajemen User</span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{ request()->is('cashier*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('cashier.index') }}">Akun Kasir</a>
        </li>
        @auth('admin')
        <li class="{{ request()->is('owner*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('owner.index') }}">Akun Pemilik</a>
        </li>
        @else 
        <li class="{{ request()->is('owner*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('owner.store', auth()->id()) }}">Akun Saya</a>
        </li>
        @endauth
        <li class="{{ request()->is('supplier*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('supplier.index') }}">Data Supplier</a>
        </li>
        @auth('owner')
        <li class="{{ request()->is('store*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('store.show', auth()->user()->store_id) }}">Toko Saya</a>
        </li>
        @endauth
    </ul>
</li>

<li class="dropdown {{ request()->is('report*') ? 'active' : '' }}">
    <a href="{{ route('report') }}" class="nav-link"><i data-feather="clipboard"></i><span>Laporan</span></a>
</li>

<li class="dropdown {{ request()->is('log*') ? 'active' : '' }}">
    <a href="{{ route('log-activity.index') }}" class="nav-link"><i data-feather="file-text"></i><span>Lihat Aktifitas</span></a>
</li>
@endguest

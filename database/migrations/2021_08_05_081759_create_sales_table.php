<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->integer('user_id');
            $table->string('cust_name');
            $table->string('type_payment')->default('Tunai');
            $table->unsignedBigInteger('total_price');
            $table->unsignedBigInteger('discount')->nullable()->default(0);
            $table->unsignedBigInteger('final_amount');
            $table->unsignedBigInteger('paid_amount');
            $table->Integer('change');
            $table->string('note')->nullable();
            $table->tinyInteger('is_completed')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}

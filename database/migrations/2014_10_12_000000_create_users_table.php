<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('store_id');
            $table->string('username')->unique();
            $table->string('fullname');
            $table->string('role');
            $table->string('password');
            $table->enum('gender', ['Pria','Wanita']);
            $table->string('religion');
            $table->string('place_birth');
            $table->date('date_birth');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};

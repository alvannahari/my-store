<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_items', function (Blueprint $table) {
            $table->id();
            $table->string('sale_id');
            $table->unsignedBigInteger('type_id')->nullable();
            $table->string('name');
            $table->string('desc')->nullable();
            $table->string('code')->nullable();
            $table->unsignedInteger('qty');
            $table->unsignedInteger('capital_price');
            $table->unsignedInteger('price');
            $table->unsignedInteger('income');
            $table->enum('status', ['succeed','returned'])->default('succeed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
}

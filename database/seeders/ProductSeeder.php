<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [1, 'LED Philips 3W', 15000],
            [1, 'LED Philips 4W Kecil', 18000],
            [1, 'LED Philips 4W Besar', 21000],
            [2, 'Steker Broco GPG', 2500],
            [2, 'Steker Broco Bulat', 9000],
            [3, 'Senter Kepala 25W Push On', 38000],
            [3, 'Senter Kepala 25W Meval', 40000],
        ];

        foreach ($data as $key => $value) {
            $product_id = 'ITEM-'.substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,12);

            DB::table('products')->insert([
                'id' => $product_id,
                'category_id' => $value[0],
                'name' => $value[1],
                'code' => substr(md5(mt_rand()), 0, 7),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            
            for ($i=0; $i < 2; $i++) { 
                DB::table('product_types')->insert([
                    'product_id' => $product_id,
                    'type' => $i == 0 ? 'Satuan' : 'Roll',
                    'capital_price' => $value[2],
                    'inventory' => random_int(10, 100),
                    'created_at' => $i == 0 ? Carbon::now() : Carbon::now()->addHour(),
                    'updated_at' => $i == 0 ? Carbon::now() : Carbon::now()->addHour(),
                ]);

                for ($j=0; $j < 2; $j++) { 
                    DB::table('product_prices')->insert([
                        'type_id' => 2 * $key + $i + 1,
                        'min' => 1,
                        'price' => $j == 0 ? $value[2] + 5000 : $value[2] + 10000,
                        'created_at' => $j == 0 ? Carbon::now() : Carbon::now()->addHour(),
                        'updated_at' => $j == 0 ? Carbon::now() : Carbon::now()->addHour(),
                    ]);
                }
            }
        }
    }
}

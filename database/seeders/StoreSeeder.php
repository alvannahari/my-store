<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('stores')->insert([
            'name' => 'AFWAJA',
            'owner' => 'OWNER AFWAJA',
            'address' => 'Pucung Lor, Ngantru',
            'mobile_number' => '08563194806',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

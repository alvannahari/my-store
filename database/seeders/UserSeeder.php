<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            "store_id" => 1,
            "username" => 'ownerafwaja',
            "fullname" => 'Owner Afwaja',
            "role" => 'owner',
            "password" => bcrypt('ownerafwaja'),
            "gender" => 'Pria',
            "religion" => 'Islam',
            "place_birth" => 'Blitar',
            "date_birth" => '1997-07-22',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            "store_id" => 1,
            "username" => 'cashierafwaja',
            "fullname" => 'Kasir Afwaja',
            'role' => 'cashier',
            "password" => bcrypt('cashierafwaja'),
            "gender" => 'Wanita',
            "religion" => 'Islam',
            "place_birth" => 'Blitar',
            "date_birth" => '1997-07-22',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

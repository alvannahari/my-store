<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CashierController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LogActivityController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductPriceController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\PurchaseImageController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ReturnProductController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TransactionController;

Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login.store');
Route::post('logout', [LoginController::class, 'logout'])->middleware('auth:admin,owner,cashier')->name('logout');
    
Route::group(['middleware' => 'auth:admin,owner,cashier'], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('home');

    Route::resource('category', CategoryController::class)->except(['edit','create']);

    Route::resource('product', ProductController::class)->except(['edit']);
    Route::get('product-others', [ProductController::class, 'others'])->name('product.others');
    Route::post('product-import', [ProductController::class, 'import'])->name('product.import');
    Route::resource('product-price', ProductPriceController::class)->only(['store', 'destroy']);
    
    Route::resource('sale', SaleController::class)->except(['edit']);
    Route::resource('transaction', TransactionController::class)->except(['index','edit']);
    Route::get('sale-trash', [SaleController::class, 'trash'])->name('sale.trash');
    
    Route::resource('purchase', PurchaseController::class)->except(['edit']);
    Route::resource('purchase-image', PurchaseImageController::class)->only(['store', 'destroy']);
    
    Route::resource('return-product', ReturnProductController::class)->except(['create','edit']);

    Route::resource('cashier', CashierController::class)->except(['create','edit']);

    Route::resource('supplier', SupplierController::class)->except(['create','edit']);

    Route::resource('owner', OwnerController::class)->except(['create','edit']);

    Route::resource('store', StoreController::class)->only(['show','update']);

    Route::get('report', [ReportController::class, 'report'])->name('report');

    Route::resource('log-activity', LogActivityController::class)->only(['index','destroy']);
});